# Raspberry PI Light Controller

_Let your PI control some lights_

## Installation

### Requirements

> Make sure you have at least Python 3.6 installed. If not, see this how-to: https://gist.github.com/dschep/24aa61672a2092246eaca2824400d37f (or also https://raspberrypi.stackexchange.com/questions/59381/how-do-i-update-my-rpi3-to-python-3-6)


Install these python dependencies:

```bash
$ pip3 install wiringpi   # For controlling GPIO's
```

> * wiringpi: http://wiringpi.com/


### Setup service

#### Light controller service

Run `./setup_service.sh` to install and enable the service (it will ask for your root password). After that, restart your system with `sudo reboot` or just use `systemctl start pi_light_controller` to start the service.

##### Control

This service can be controlled by the `systemctl` command:

* `systemctl start pi_light_controller`: Start the service
* `systemctl status pi_light_controller`: Get the current status (logging) of the server
* `systemctl stop pi_light_controller`: Stop the service
* `systemctl enable pi_light_controller`: Start the service during boot
* `systemctl disable pi_light_controller`: Stop starting the service during boot

##### Monitoring

Also, `journalctl` can be used for monitoring the logs:

```bash
$ journalctl -u pi_light_controller.service
```

Add flags like `-e` for scrolling to the end or `-f` for live log following.

Complete logfile can be found in `/var/log/syslog`


## Run

```bash
$ python3 app.py
```

or using the whole raspberypi_bluetooth module:
```bash
. venv/bin/activate
python entrypoint.py light_controller
```

### Usage

The service only responds to commands send to its queue/topic in the MQTT broker. This topic's routing key/name can be found in [settings.py](settings.py): `MQTT_TOPIC`. 

#### Commands

| Command | Description |
| --- | --- |
| **Desk light** | |
| `desk brightness <int:value> [float:duration] [float:timeout]` | Set desk brightness to value (0-4095) for optional duration (seconds) and after an optional timeout (seconds) |
| **MovingHead** | |
| `movinghead on [float:duration] [float:timeout]` | Set movinghead on to value (0-255) for optional duration (seconds) and after an optional timeout (seconds) |
| `movinghead off [float:duration] [float:timeout]` | Set movinghead off to value (0-255) for optional duration (seconds) and after an optional timeout (seconds) |
| `movinghead (brightness;level) <int:value> [float:duration] [float:timeout]` | Set movinghead brightness to value (0-255) for optional duration (seconds) and after an optional timeout (seconds) |
| `movinghead color <str:r> <str:g> <str:b> [float:duration] [float:timeout]` | Set movinghead color to value (0-255) for optional duration (seconds) and after an optional timeout (seconds) |
| `movinghead effect speed <int:value> [float:duration] [float:timeout]` | Set movinghead effect_speed to value (0-255) for optional duration (seconds) and after an optional timeout (seconds) |
| `movinghead effect <str:name> [float:timeout]` | Set movinghead effect to value after an optional timeout (seconds) |
| `movinghead pitch <int:value> [float:duration] [float:timeout]` | Set movinghead pitch to value (0-255) for optional duration (seconds) and after an optional timeout (seconds) |
| `movinghead rotation <int:value> [float:duration] [float:timeout]` | Set movinghead rotation to value (0-255) for optional duration (seconds) and after an optional timeout (seconds) |

Moving head effects:
- OFF
- SOLID
- STROBE
- STARS
- SMILEY
- RAINBOW

*Example*
Set desk brightness to 100% during 5 seconds: `desk brightness 4095 5`

Using bash: 
```bash
# For routing key, see light_controller.settings.MQTT_TOPIC"
rabbitmqadmin publish exchange=amq.topic routing_key="light_controller" payload="desk brightness 4095 5"
```

### Tests
I am a bad man and have made no tests :(

## Future

#### Nice to have's

* none yet