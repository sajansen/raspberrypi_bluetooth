import logging
import random
from typing import List, Optional
import multiprocessing

import smbus2

from light_controller import settings
from light_controller.utils import sleep_ms

logger = logging.getLogger(__name__)

queue = multiprocessing.Queue()

bus = smbus2.SMBus(1)

_process: Optional[multiprocessing.Process] = None


def start() -> multiprocessing.Process:
    global _process
    logger.info("Starting i2c listening process")
    _process = multiprocessing.Process(target=_listen,
                                       args=(queue,),
                                       daemon=True)
    _process.start()
    return _process


def _listen(queue: multiprocessing.Queue):
    error_count = 0
    while True:
        if error_count > 30:
            logger.error("To many i2c errors ({}), stopping server".format(error_count))
            return

        try:
            command = queue.get(block=True)
        except Exception as e:
            logger.error("Failed to get next element from queue")
            logger.exception(e, exc_info=True)
            error_count = error_count + 1
            continue
        error_count = 0

        try:
            _send(command[0], command[1], command[2])
        except Exception as e:
            logger.error("Caught exception during I2C send")
            logger.exception(e, exc_info=True)


def _send(address: int, register: int, data: List, max_retries: int = 9):
    try:
        bus.write_i2c_block_data(address, register, data)

    except OSError as e:
        if max_retries <= 0:
            logger.error("Failed to write data to I2C bus. ")
            logger.exception(e, exc_info=True)
            return False

        logger.error("Failed to write data to I2C bus. Retrying ({})...".format(max_retries))
        sleep_ms(random.randint(1, 10) / 10000)  # sleep between 0.1 and 1 milliseconds
        return _send(address, register, data, max_retries - 1)

    # Give the i2c bus time to process it
    sleep_ms(milliseconds=settings.I2C_WRITE_DELAY_MS)


def send(queue: multiprocessing.Queue, address: int, register: int, data: List):
    queue.put((address, register, data,))
