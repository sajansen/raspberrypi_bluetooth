import logging
import paho.mqtt.client as mqtt

from light_controller import settings

logger = logging.getLogger(__name__)


def send_command(client: mqtt.Client, command: str):
    logger.debug("Sending '{}' to MQTT topic '{}'".format(command, settings.MQTT_TOPIC))
    client.publish(settings.MQTT_TOPIC, command)


def set_brightness(client: mqtt.Client, level: int = 0, duration: float = 0.0, timeout: float = 0.0):
    send_command(client, "desk brightness {} {} {}".format(level, duration, timeout))
