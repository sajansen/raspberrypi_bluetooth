import logging
import multiprocessing
import random
import time
from typing import Optional

from light_controller import settings, utils
from light_controller.i2c import i2c
from light_controller.utils import handle_timeout, sleep_ms

logger = logging.getLogger(__name__)

current_level = multiprocessing.Value("i", -1)
current_process: Optional[multiprocessing.Process] = None

_i2c_queue = i2c.queue


def setup() -> None:
    logger.debug("Setting up light controller")

    PWM_CONTROLLER_MODE1_RESTART = 7
    PWM_CONTROLLER_MODE1_EXTCLK = 6
    PWM_CONTROLLER_MODE1_AI = 5
    PWM_CONTROLLER_MODE1_SLEEP = 4
    PWM_CONTROLLER_MODE1_SUB1 = 3
    PWM_CONTROLLER_MODE1_SUB2 = 2
    PWM_CONTROLLER_MODE1_SUB3 = 1
    PWM_CONTROLLER_MODE1_ALLCALL = 0

    mode1_register_value = (0 << PWM_CONTROLLER_MODE1_RESTART) | \
                           (0 << PWM_CONTROLLER_MODE1_EXTCLK) | \
                           (1 << PWM_CONTROLLER_MODE1_AI) | \
                           (0 << PWM_CONTROLLER_MODE1_SLEEP) | \
                           (0 << PWM_CONTROLLER_MODE1_SUB1) | \
                           (0 << PWM_CONTROLLER_MODE1_SUB2) | \
                           (0 << PWM_CONTROLLER_MODE1_SUB3) | \
                           (1 << PWM_CONTROLLER_MODE1_ALLCALL)

    logger.debug("Setting PWM controller mode to: {}".format(mode1_register_value))
    i2c.send(_i2c_queue, settings.PWM_CONTROLLER_ADDRESS, settings.PWM_CONTROLLER_MODE1, [
        mode1_register_value
    ])

    set_brightness(0)


def set_brightness(level: int = 0, duration: float = 0.0, timeout: float = 0.0) -> None:
    global current_process

    if not settings.DESK_ENABLE:
        return logger.info("Cannot set brightness, desk led disabled")

    logger.info("Setting brightness to {} during {} seconds, after {} seconds".format(level, duration, timeout))

    current_process = utils.start_process(current_process=current_process,
                                          process_function=brightness_process,
                                          arguments=(_i2c_queue, level, duration, timeout),
                                          logger=logger)
    time.sleep(0.01)


def brightness_process(_i2c_queue: multiprocessing.Queue,
                       target_value: int,
                       duration: float = 0.0,
                       timeout: float = 0.0) -> None:
    logger.debug("Brightness process initial values: current level={}, target level={}"
                 .format(current_level.value, target_value))

    handle_timeout(__name__, timeout)

    # Check if fading is necessary
    if duration <= 0.0:
        logger.info("Directly set level because duration is 0")
        _set_level(_i2c_queue, target_value)
        return

    if target_value == current_level.value:
        logger.info("Current level is already target level")
        _set_level(_i2c_queue, target_value)
        return

    level_diff = abs(target_value - current_level.value)
    previous_percentage = -1
    sleep_time_ms = duration / level_diff * 1000

    # Do the fading
    for i in range(0, level_diff):
        new_value = current_level.value + (1 if target_value > current_level.value else -1)
        _set_level(_i2c_queue, new_value)

        # Some percentage logging
        percentage = round(current_level.value / settings.MAX_BRIGHTNESS * 100)
        if percentage != previous_percentage:
            previous_percentage = percentage
            logger.debug("  brightness  {:3}%".format(percentage))

        sleep_ms(sleep_time_ms)


def _set_level(_i2c_queue: multiprocessing.Queue, level: int) -> None:
    global current_level

    curved_level = round(_convert_to_curve(level))

    _send_data(_i2c_queue, curved_level)
    current_level.value = level


def _convert_to_curve(value: float,
                      curve_factor: float = settings.BRIGHTNESS_CURVE_FACTOR,
                      min_input: float = 0,
                      max_input: float = settings.MAX_BRIGHTNESS,
                      min_output: float = 0,
                      max_output: float = settings.PWM_CONTROLLER_MAX_VALUE) -> float:
    if value <= min_input:
        return min_output
    elif value >= max_input:
        return max_output

    x = value / max_input
    y = x ** curve_factor
    return y * max_output


def _send_data(_i2c_queue: multiprocessing.Queue, data: int, max_retries: int = 9) -> bool:
    """
    Sends data to the I2C bus, to the default I2C address. Retries if fails
    :param data: Data array to be send
    :param max_retries: Number of retries left
    :return: True if transmission was successful, otherwise False
    """

    if data > settings.PWM_CONTROLLER_MAX_VALUE:
        data = settings.PWM_CONTROLLER_MAX_VALUE

    if data < 0:
        data = 0

    red = data
    green = int(round(red * settings.RED_GREEN_MIX))

    low_byte_r = red & 255
    high_byte_r = (red >> 8) & 255
    low_byte_g = green & 255
    high_byte_g = (green >> 8) & 255

    if data == 0:
        high_byte_r = 16
        high_byte_g = 16

    try:
        i2c.send(_i2c_queue,
                 settings.PWM_CONTROLLER_ADDRESS,
                 settings.PWM_CONTROLLER_LED12_OFF_L,
                 [low_byte_r, high_byte_r, 0, 0, low_byte_g, high_byte_g])
        return True
    except OSError as e:
        if max_retries <= 0:
            logger.error("Failed to write data to I2C bus. ")
            logger.exception(e, exc_info=True)
            return False

        logger.error("Failed to write data to I2C bus. Retrying ({})...".format(max_retries))
        sleep_ms(random.randint(5, 30) / 10000)  # sleep between 0.5 and 3 milliseconds
        return _send_data(_i2c_queue, data, max_retries - 1)
