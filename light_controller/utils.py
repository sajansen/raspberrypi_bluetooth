import logging
import multiprocessing
import time
from typing import Optional, Callable, Tuple


def handle_timeout(caller: str = __name__, remaining_time: float = 0.0):
    logger = logging.getLogger(caller)

    if remaining_time == 0.0:
        return

    if remaining_time < 10:
        time.sleep(remaining_time)
        return

    if remaining_time < 120:
        log_interval = 10
        while remaining_time >= log_interval:
            logger.debug("{} seconds left...".format(remaining_time))
            time.sleep(log_interval)
            remaining_time -= log_interval

        return handle_timeout(caller, remaining_time)

    if remaining_time < 3600:
        log_interval = 60
        while remaining_time >= log_interval:
            logger.debug("{} minutes left...".format(remaining_time // 60))
            time.sleep(log_interval)
            remaining_time -= log_interval

        return handle_timeout(caller, remaining_time)

    log_interval = 15 * 60
    while remaining_time >= log_interval:
        logger.debug("{} minutes left...".format(remaining_time // 60))
        time.sleep(log_interval)
        remaining_time -= log_interval

    return handle_timeout(caller, remaining_time)


def kill_process(process: Optional[multiprocessing.Process], logger):
    process.terminate()
    process.join(timeout=1)

    if not process.is_alive():
        return

    logger.info("Process still alive. Killing current running process")
    process.kill()
    time.sleep(0.01)


def start_process(current_process: Optional[multiprocessing.Process],
                  process_function: Callable, arguments: Tuple, logger) -> multiprocessing.Process:
    """
    Start a new process of the given kind with the given arguments. If the current process is still running,
    it will be terminated
    :param current_process: The variable which holds the current (running) process
    :param process_function: The function the process must execute
    :param arguments: Arguments to pass to the process_function
    :param logger: Logger to log information to
    :return:
    """
    if current_process is not None:
        # Log that the current process is actually still running (we don't really need to do something with this
        # information, because we're going to kill it anyway)
        if current_process.is_alive():
            logger.info("Terminating current running process")

        kill_process(current_process, logger)

    current_process = multiprocessing.Process(target=process_function,
                                              args=arguments,
                                              daemon=True)

    logger.info("Starting new process")
    current_process.start()
    return current_process


def sleep_ms(milliseconds: float):
    target_time = time.time() + milliseconds / 1000.0
    while time.time() < target_time:
        pass
