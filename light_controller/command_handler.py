import logging

from common.command_handler import find_matching_command, Command
from light_controller.commands import desk_led, movinghead

logger = logging.getLogger(__name__)


def handle_command(text: str) -> None:
  command = find_matching_command(commands, text)

  if command is None:
    logger.info("Unknown command. Do you know what you are doing?")
    return

  command.execute(text)


commands = [
  # Light
  Command(r"^desk brightness <int:value> ?<float:duration>? ?<float:timeout>?", desk_led.brightness),

  # MovingHead
  Command(r"^(movinghead|mh) on ?<float:duration>? ?<float:timeout>?", movinghead.on),
  Command(r"^(movinghead|mh) off ?<float:duration>? ?<float:timeout>?", movinghead.off),
  Command(r"^(movinghead|mh) (brightness|level) <int:value> ?<float:duration>? ?<float:timeout>?", movinghead.brightness),
  Command(r"^(movinghead|mh) color <str:r> <str:g> <str:b> ?<float:duration>? ?<float:timeout>?", movinghead.color),
  Command(r"^(movinghead|mh) effect speed <int:value> ?<float:duration>? ?<float:timeout>?", movinghead.effect_speed),
  Command(r"^(movinghead|mh) effect <str:name> ?<float:timeout>?", movinghead.effect),
  Command(r"^(movinghead|mh) pitch <int:value> ?<float:duration>? ?<float:timeout>?", movinghead.pitch),
  Command(r"^(movinghead|mh) rotation <int:value> ?<float:duration>? ?<float:timeout>?", movinghead.rotation),
]
