import logging

from light_controller import server

logger = logging.getLogger(__name__)
logging.basicConfig(
  format='[%(levelname)s] %(asctime)s %(name)s | %(message)s',
  level=logging.DEBUG)


def run():
  client = server.start()
  try:
    client.loop_forever()
  except KeyboardInterrupt:
    pass

  server.stop(client)
  logger.info("Light controller server stopped.")


if __name__ == '__main__':
  run()
