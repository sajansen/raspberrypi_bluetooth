#!/bin/bash

# Prepare service file
echo "Creating service"
SCRIPTPATH="$(
  cd "$(dirname "$0")"
  pwd -P
)"
PARENTPATH="$(dirname ${SCRIPTPATH})"
cat "${SCRIPTPATH}/pi_light_controller.service.template" | sed "s#\/directorypath#${PARENTPATH}#g" >pi_light_controller.service

# Deploy service file
echo "Deploying service"
sudo cp pi_light_controller.service /etc/systemd/system/
sudo chmod 664 /etc/systemd/system/pi_light_controller.service

# Clean up our garbage
echo "Cleaning up"
rm pi_light_controller.service

# Run service file
echo "Enabling service at boot"
sudo systemctl enable pi_light_controller # Run at boot

echo "Done :)"
