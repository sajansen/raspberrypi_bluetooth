import multiprocessing

from light_controller.i2c import i2c

MH_CON_META_START_BITS = 0x05

MH_CMD_POSITION_X = 1
MH_CMD_POSITION_Y = 2
MH_CMD_COLOR_R = 3
MH_CMD_COLOR_G = 4
MH_CMD_COLOR_B = 5
MH_CMD_EFFECT = 6
MH_CMD_EFFECT_SPEED = 7
MH_CMD_BRIGHTNESS = 8
MH_CMD_CONNECTION = 10
MH_CMD_DEMO = 11


def send(i2c_queue: multiprocessing.Queue, address: int, command: int, data: int) -> None:
  meta_byte = (MH_CON_META_START_BITS << 5) | command
  i2c.send(i2c_queue, address, meta_byte, [data])
