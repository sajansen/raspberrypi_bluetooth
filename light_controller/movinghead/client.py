import logging

from light_controller import settings
import paho.mqtt.client as mqtt

logger = logging.getLogger(__name__)


def send_command(client: mqtt.Client, command: str):
    logger.debug("Sending '{}' to MQTT topic '{}'".format(command, settings.MQTT_TOPIC))
    client.publish(settings.MQTT_TOPIC, command)


def get_max_brightness():
    return settings.MH_MAX_BRIGHTNESS


def set_color(client: mqtt.Client, r: int = 0, g: int = 0, b: int = 0, duration: float = 0.0, timeout: float = 0.0):
    send_command(client, "movinghead color {} {} {} {} {}".format(r, g, b, duration, timeout))


def set_effect(client: mqtt.Client, value: str = "OFF", timeout: float = 0.0):
    send_command(client, "movinghead effect {} {}".format(value, timeout))


def set_effect_speed(client: mqtt.Client, value: int = 0, duration: float = 0.0, timeout: float = 0.0):
    send_command(client, "movinghead effect speed {} {} {}".format(value, duration, timeout))


def set_brightness(client: mqtt.Client, value: int = 0, duration: float = 0.0, timeout: float = 0.0):
    send_command(client, "movinghead brightness {} {} {}".format(value, duration, timeout))


def set_rotation(client: mqtt.Client, value: int = 0, duration: float = 0.0, timeout: float = 0.0):
    send_command(client, "movinghead rotation {} {} {}".format(value, duration, timeout))


def set_pitch(client: mqtt.Client, value: int = 0, duration: float = 0.0, timeout: float = 0.0):
    send_command(client, "movinghead pitch {} {} {}".format(value, duration, timeout))


def set_off(client: mqtt.Client, duration: float = 0.0, timeout: float = 0.0):
    send_command(client, "movinghead off {} {}".format(duration, timeout))


def set_on(client: mqtt.Client, duration: float = 0.0, timeout: float = 0.0):
    send_command(client, "movinghead on {} {}".format(duration, timeout))
