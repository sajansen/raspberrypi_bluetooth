import enum


class RGB:
  def __init__(self, r: int = 0,
               g: int = 0,
               b: int = 0):
    self.r = r
    self.g = g
    self.b = b

  def equals(self, color) -> bool:
    return self.r == color.r \
           and self.g == color.g \
           and self.b == color.b

  def __repr__(self):
    return "RGB({}, {}, {})".format(self.r, self.g, self.b)


class Effect(enum.Enum):
  OFF = 0
  SOLID = 1
  STROBE = 2
  STARS = 3
  SMILEY = 4
  RAINBOW = 5
