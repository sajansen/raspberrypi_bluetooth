import logging
import multiprocessing
import random
import time
from typing import Optional, Any, Callable

from light_controller import settings, utils
from light_controller.movinghead.communication_protocol import *
from light_controller.movinghead.objects import RGB, Effect
from light_controller.utils import handle_timeout, sleep_ms

logger = logging.getLogger(__name__)

current_brightness = multiprocessing.Value("i", -1)
current_color_r = multiprocessing.Value("i", -1)
current_color_g = multiprocessing.Value("i", -1)
current_color_b = multiprocessing.Value("i", -1)
current_effect = multiprocessing.Value("i", -1)
current_effect_speed = multiprocessing.Value("i", -1)
current_pitch = multiprocessing.Value("i", -1)
current_rotation = multiprocessing.Value("i", -1)

current_brightness_process: Optional[multiprocessing.Process] = None
current_color_process_r: Optional[multiprocessing.Process] = None
current_color_process_g: Optional[multiprocessing.Process] = None
current_color_process_b: Optional[multiprocessing.Process] = None
current_effect_process: Optional[multiprocessing.Process] = None
current_effect_speed_process: Optional[multiprocessing.Process] = None
current_pitch_process: Optional[multiprocessing.Process] = None
current_rotation_process: Optional[multiprocessing.Process] = None

_i2c_queue = i2c.queue


def setup():
    logger.debug("Setting up moving head")
    pass


def _spawn_process(current_process: Optional[multiprocessing.Process],
                   process_function: Callable,
                   value: Any, duration: float = 0.0,
                   timeout: float = 0.0) -> multiprocessing.Process:
    """
    Start a new process of the given kind with the given arguments. If the current process is still running,
    it will be terminated
    :param current_process: The variable which holds the current (running) process
    :param process_function: The function the process must execute
    :param value: The value for the process function
    :param duration: The duration for the process function
    :return:
    """

    return utils.start_process(current_process=current_process,
                               process_function=process_function,
                               arguments=(_i2c_queue, value, duration, timeout),
                               logger=logger)


def set_brightness(value: int = 0, duration: float = 0.0, timeout: float = 0.0) -> None:
    global current_brightness_process

    if not settings.MOVING_HEAD_ENABLE:
        return logger.info("Cannot set brightness, moving head disabled")

    logger.info("Setting brightness to: {}".format(value))
    current_brightness_process = _spawn_process(current_brightness_process, brightness_process, value, duration,
                                                timeout)


def set_color(color: RGB, duration: float = 0.0, timeout: float = 0.0) -> None:
    global current_color_process_r, current_color_process_g, current_color_process_b

    if not settings.MOVING_HEAD_ENABLE:
        return logger.info("Cannot set color, moving head disabled")

    logger.info("Setting color to: {}".format(color))
    current_color_process_r = _spawn_process(current_color_process_r, color_process_r, color, duration, timeout)
    current_color_process_g = _spawn_process(current_color_process_g, color_process_g, color, duration, timeout)
    current_color_process_b = _spawn_process(current_color_process_b, color_process_b, color, duration, timeout)


def set_effect(effect: Effect, timeout: float = 0.0) -> None:
    global current_effect_process

    if not settings.MOVING_HEAD_ENABLE:
        return logger.info("Cannot set effect, moving head disabled")

    logger.info("Setting effect to: {}".format(effect.name))
    current_effect_process = _spawn_process(current_effect_process,
                                            effect_process,
                                            effect, 0, timeout)


def set_effect_speed(value: int = 0, duration: float = 0.0, timeout: float = 0.0) -> None:
    global current_effect_speed_process

    if not settings.MOVING_HEAD_ENABLE:
        return logger.info("Cannot set effect_speed, moving head disabled")

    logger.info("Setting effect_speed to: {}".format(value))
    current_effect_speed_process = _spawn_process(current_effect_speed_process,
                                                  effect_speed_process,
                                                  value, duration, timeout)


def set_pitch(value: int = 0, duration: float = 0.0, timeout: float = 0.0) -> None:
    global current_pitch_process

    if not settings.MOVING_HEAD_ENABLE:
        return logger.info("Cannot set pitch, moving head disabled")

    logger.info("Setting pitch to: {}".format(value))
    current_pitch_process = _spawn_process(current_pitch_process, pitch_process, value, duration, timeout)


def set_rotation(value: int = 0, duration: float = 0.0, timeout: float = 0.0) -> None:
    global current_rotation_process

    if not settings.MOVING_HEAD_ENABLE:
        return logger.info("Cannot set rotation, moving head disabled")

    logger.info("Setting rotation to: {}".format(value))
    current_rotation_process = _spawn_process(current_rotation_process, rotation_process, value, duration, timeout)


def _int_fade_process(i2c_queue: multiprocessing.Queue,
                      set_function: Callable, 
                      current_value: multiprocessing.Value,
                      target_value: int, duration: float = 0.0,
                      max_value: int = 255,
                      label: str = "") -> None:
    """
    Fades to a certain target_value for integer types. The new value will be set by calling the `set_function`
    with the new value as argument.
    :param set_function: Function to call which will send the new value to the I2C bus
    :param current_value: The current value object
    :param target_value: The target value to reach
    :param duration: The duration of the whole execution of reaching the target value
    :param max_value: The maximum value possible, used for logging only
    :return:
    """

    # Check if fading is necessary
    if duration <= 0.0 or target_value == current_value.value:
        set_function(i2c_queue, target_value)
        return

    level_diff = abs(target_value - current_value.value)
    previous_percentage = -1
    sleep_time_ms = duration / level_diff * 1000
    
    # Do the fading
    for i in range(0, level_diff):
        new_value = current_value.value + (1 if target_value > current_value.value else -1)
        set_function(i2c_queue, new_value)

        # Some percentage logging
        percentage = round(current_value.value / max_value * 100)
        if percentage != previous_percentage:
            previous_percentage = percentage
            logger.debug("  {}  {:3}%".format(label, percentage))

        sleep_ms(sleep_time_ms)


def brightness_process(i2c_queue: multiprocessing.Queue, target_value: int, duration: float = 0.0, timeout: float = 0.0) -> None:
    logger.debug("Brightness process initial values: current brightness={}, target brightness={}"
                 .format(current_brightness.value, target_value))

    handle_timeout(__name__, timeout)

    _int_fade_process(i2c_queue, _set_brightness, current_brightness, target_value, duration,
                      max_value=settings.MH_MAX_BRIGHTNESS, label="brightness")


def color_process_r(i2c_queue: multiprocessing.Queue, target_color: RGB, duration: float = 0.0, timeout: float = 0.0) -> None:
    current_color = RGB(current_color_r.value, current_color_g.value, current_color_b.value)
    logger.debug("Color process RED initial values: current color={}, target color={}"
                 .format(current_color, target_color))

    handle_timeout(__name__, timeout)

    _int_fade_process(i2c_queue, _set_color_r, current_color_r, target_color.r, duration,
                      max_value=settings.MH_MAX_COLOR_VALUE, label="color r")


def color_process_g(i2c_queue: multiprocessing.Queue, target_color: RGB, duration: float = 0.0, timeout: float = 0.0) -> None:
    current_color = RGB(current_color_r.value, current_color_g.value, current_color_b.value)
    logger.debug("Color process GREEN initial values: current color={}, target color={}"
                 .format(current_color, target_color))

    handle_timeout(__name__, timeout)

    _int_fade_process(i2c_queue, _set_color_g, current_color_g, target_color.g, duration,
                      max_value=settings.MH_MAX_COLOR_VALUE, label="color g")


def color_process_b(i2c_queue: multiprocessing.Queue, target_color: RGB, duration: float = 0.0, timeout: float = 0.0) -> None:
    current_color = RGB(current_color_r.value, current_color_g.value, current_color_b.value)
    logger.debug("Color process BLUE initial values: current color={}, target color={}"
                 .format(current_color, target_color))

    handle_timeout(__name__, timeout)

    _int_fade_process(i2c_queue, _set_color_b, current_color_b, target_color.b, duration,
                      max_value=settings.MH_MAX_COLOR_VALUE, label="color b")


def effect_process(i2c_queue: multiprocessing.Queue, target_effect: Effect, duration: float = 0.0, timeout: float = 0.0) -> None:
    logger.debug("Effect process initial values: current effect={}, target effect={}"
                 .format(current_effect.value, target_effect))

    handle_timeout(__name__, timeout)

    _set_effect(i2c_queue, target_effect)


def effect_speed_process(i2c_queue: multiprocessing.Queue, target_value: int, duration: float = 0.0, timeout: float = 0.0) -> None:
    logger.debug("Effect speed process initial values: current effect speed={}, target effect speed={}"
                 .format(current_effect_speed.value, target_value))

    handle_timeout(__name__, timeout)

    _int_fade_process(i2c_queue, _set_effect_speed, current_effect_speed, target_value, duration,
                      max_value=settings.MH_MAX_EFFECT_SPEED, label="effect speed")


def pitch_process(i2c_queue: multiprocessing.Queue, target_value: int, duration: float = 0.0, timeout: float = 0.0) -> None:
    logger.debug("Pitch process initial values: current pitch={}, target pitch={}"
                 .format(current_pitch.value, target_value))

    handle_timeout(__name__, timeout)

    _int_fade_process(i2c_queue, _set_pitch, current_pitch, target_value, duration, max_value=settings.MH_MAX_PITCH, label="pitch")


def rotation_process(i2c_queue: multiprocessing.Queue, target_value: int, duration: float = 0.0, timeout: float = 0.0) -> None:
    logger.debug("Rotation process initial values: current rotation={}, target rotation={}"
                 .format(current_rotation.value, target_value))

    handle_timeout(__name__, timeout)

    _int_fade_process(i2c_queue, _set_rotation, current_rotation, target_value, duration,
                      max_value=settings.MH_MAX_ROTATION, label="rotation")


def _send_data(i2c_queue: multiprocessing.Queue,
               command: int, data: int, max_retries: int = 9) -> bool:
    """
    Sends data to the I2C bus, to the default MovingHead address. Retries if fails
    :param data: Data array to be send
    :param max_retries: Number of retries left
    :return: True if transmission was successful, otherwise False
    """
    try:
        send(i2c_queue, settings.MOVINGHEAD_ADDRESS, command, data)
        return True
    except OSError as e:
        if max_retries <= 0:
            logger.error("Failed to write data to I2C bus. ")
            logger.exception(e, exc_info=True)
            return False

        logger.error("Failed to write data to I2C bus. Retrying ({})...".format(max_retries))
        sleep_ms(random.randint(1, 10) / 10000)  # sleep between 0.1 and 1 milliseconds
        return _send_data(i2c_queue, data, max_retries - 1)


def _set_brightness(i2c_queue: multiprocessing.Queue, level: int) -> None:
    if _send_data(i2c_queue, MH_CMD_BRIGHTNESS, level):
        current_brightness.value = level


def _set_color_r(i2c_queue: multiprocessing.Queue, level: int) -> None:
    if _send_data(i2c_queue, MH_CMD_COLOR_R, level):
        current_color_r.value = level


def _set_color_g(i2c_queue: multiprocessing.Queue, level: int) -> None:
    if _send_data(i2c_queue, MH_CMD_COLOR_G, level):
        current_color_g.value = level


def _set_color_b(i2c_queue: multiprocessing.Queue, level: int) -> None:
    if _send_data(i2c_queue, MH_CMD_COLOR_B, level):
        current_color_b.value = level


def _set_effect(i2c_queue: multiprocessing.Queue, effect: Effect) -> None:
    if _send_data(i2c_queue, MH_CMD_EFFECT, effect.value):
        current_effect.value = effect.value


def _set_effect_speed(i2c_queue: multiprocessing.Queue, level: int) -> None:
    if _send_data(i2c_queue, MH_CMD_EFFECT_SPEED, level):
        current_effect_speed.value = level


def _set_pitch(i2c_queue: multiprocessing.Queue, level: int) -> None:
    if _send_data(i2c_queue, MH_CMD_POSITION_Y, level):
        current_pitch.value = level


def _set_rotation(i2c_queue: multiprocessing.Queue, level: int) -> None:
    if _send_data(i2c_queue, MH_CMD_POSITION_X, level):
        current_rotation.value = level
