import logging
import sys

import paho.mqtt.client as mqtt

from light_controller import settings
from light_controller.command_handler import handle_command
from light_controller.desk_led import desk_led
from light_controller.i2c import i2c
from light_controller.movinghead import movinghead

logger = logging.getLogger(__name__)


def start() -> mqtt.Client:
    logger.info("Starting light controller server")
    client = connect_to_mqtt()

    i2c.start()

    desk_led.setup()
    movinghead.setup()
    return client


def stop(client: mqtt.Client):
    logger.info("Stopping MQTT connection")
    client.disconnect()


def connect_to_mqtt() -> mqtt.Client:
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.username_pw_set(settings.MQTT_USERNAME, settings.MQTT_PASSWORD)
    client.connect(settings.MQTT_HOST, settings.MQTT_PORT, 60)

    logger.info("MQTT connection created on {}".format(settings.MQTT_HOST))
    return client


# The callback for when the client receives a CONNACK response from the server.
def on_connect(client: mqtt.Client, userdata, flags, rc: int):
    if rc == mqtt.CONNACK_ACCEPTED:
        logger.info("Connected to MQTT broker")
    else:
        logger.warning("Connection result code ({}): {}".format(rc, mqtt.connack_string(rc)))
        sys.exit("Failed to connect to MQTT broker: {}".format(mqtt.connack_string(rc)))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe(settings.MQTT_TOPIC, 0)


# The callback for when a PUBLISH message is received from the server.
def on_message(client: mqtt.Client, userdata, msg: mqtt.MQTTMessage):
    """
    Listen MQTT for incoming commands and execute these
    :return:
    """
    payload = msg.payload.decode("utf-8")
    logger.debug("MQTT message: [{}] {}".format(msg.topic, payload))

    logger.debug("Received command: {}".format(payload))

    try:
        handle_command(payload.strip())
    except Exception as e:
        logger.error("Failed to execute command '{}'".format(payload))
        logger.exception(e, exc_info=True)


if __name__ == '__main__':
    print("Do not call server.py directly. Write to the MQTT topic '{}' to send commands.".format(settings.MQTT_TOPIC))
