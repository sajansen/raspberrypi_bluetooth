import logging

from light_controller.movinghead import movinghead
from light_controller.movinghead.objects import Effect, RGB

logger = logging.getLogger(__name__)


def on(duration: str = "0.0", timeout: str = "0.0") -> None:
    logger.info("Turning movinghead on")
    duration = float(duration)
    timeout = float(timeout)

    movinghead.set_color(RGB(255, 100, 20), duration, timeout)
    movinghead.set_rotation(150, 2, timeout)
    movinghead.set_pitch(45, 2, timeout)
    movinghead.set_effect(Effect.SOLID, timeout)
    movinghead.set_brightness(200, duration, timeout)


def off(duration: str = "0.0", timeout: str = "0.0") -> None:
    logger.info("Turning movinghead off")
    duration = float(duration)
    timeout = float(timeout)

    movinghead.set_effect(Effect.OFF, duration + timeout)
    movinghead.set_brightness(0, duration, timeout)


def brightness(value: str, duration: str = "0.0", timeout: str = "0.0") -> None:
    logger.info("Setting brightness to {} for {} seconds".format(value, duration))
    movinghead.set_brightness(int(value), float(duration), float(timeout))


def color(r: str, g: str, b: str, duration: str = "0.0", timeout: str = "0.0") -> None:
    new_color = RGB(int(r), int(g), int(b))
    logger.info("Setting color to {} for {} seconds".format(new_color, duration))
    movinghead.set_color(new_color, float(duration), float(timeout))


def effect_speed(value: str, duration: str = "0.0", timeout: str = "0.0") -> None:
    logger.info("Setting effect speed to {} for {} seconds".format(value, duration))
    movinghead.set_effect_speed(int(value), float(duration), float(timeout))


def effect(name: str, timeout: str = "0.0") -> None:
    new_effect = Effect.OFF
    try:
        new_effect = Effect[name.upper()]
    except KeyError as e:
        logger.warning("Effect '{}' doesn't exists".format(name))

    logger.info("Setting effect to {} after {} seconds".format(new_effect.name, timeout))
    movinghead.set_effect(new_effect, float(timeout))


def pitch(value: str, duration: str = "0.0", timeout: str = "0.0") -> None:
    logger.info("Setting pitch to {} for {} seconds".format(value, duration))
    movinghead.set_pitch(int(value), float(duration), float(timeout))


def rotation(value: str, duration: str = "0.0", timeout: str = "0.0") -> None:
    logger.info("Setting rotation to {} for {} seconds".format(value, duration))
    movinghead.set_rotation(int(value), float(duration), float(timeout))
