import logging

from light_controller.desk_led import desk_led

logger = logging.getLogger(__name__)


def brightness(value: str, duration: str = "0.0", timeout: float = "0.0") -> None:
  logger.info("Setting brightness to {} for {} seconds".format(value, duration))
  desk_led.set_brightness(int(value), float(duration), float(timeout))
