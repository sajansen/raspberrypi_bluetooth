import logging
import sys
from typing import Optional

import bluetooth

from bluetooth_server import settings
from bluetooth_server.commands import system
from bluetooth_server.bluetooth_client import Client
from bluetooth_server.command_handler import handle_command
from bluetooth_server.mqtt_connection import mqtt_connect, mqtt_disconnect

logger = logging.getLogger(__name__)

server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
client: Optional[Client] = None


def start():
  logger.info("Starting server...")
  server_sock.bind(("", bluetooth.PORT_ANY))
  server_sock.listen(1)
  port = server_sock.getsockname()[1]

  try:
    bluetooth.advertise_service(server_sock, settings.SERVER_NAME, service_id=settings.SERVER_UUID,
                                service_classes=[settings.SERVER_UUID, bluetooth.SERIAL_PORT_CLASS],
                                profiles=[bluetooth.SERIAL_PORT_PROFILE])
  except bluetooth.btcommon.BluetoothError as e:
    logger.exception(e, exc_info=True)

    if len(e.args) >= 2 and e.args[1] == "(2, 'No such file or directory')":
      system.restart_bluetooth()

    sys.exit(1)

  logger.info("Server listening on port {}".format(port))

  mqtt_connect()


def handle_connections():
  """
  Function which continuously (in a loop) listens for incoming connections and execute commands for these connections
  :return:
  """
  global client
  logger.debug("Waiting for connection on RFCOMM channel {}".format(server_sock.getsockname()[1]))

  client = accept_connection()

  if not client:
    logger.error("Client is None")
    return

  try:
    handle_commands(client)
  except KeyboardInterrupt:
    return False
  except Exception as e:
    logger.exception(e, exc_info=True)


def accept_connection() -> Client:
  client_sock, client_info = server_sock.accept()
  new_client = Client(client_sock, client_info)
  logger.info("Accepted connection from {}".format(new_client.info))
  return new_client


def handle_commands(client: Client):
  """
  Listen for commands send by the client and execute these commands
  :param client:
  :return:
  """
  while True:
    data = client.sock.recv(1024)

    if not data:
      logger.info("Client has disconnected")
      return

    for command in data.decode().strip().split("\n"):
      logger.debug("Received command: {}".format(command))

      try:
        handle_command(client, command.strip())
      except Exception as e:
        logger.error("Failed to execute command '{}'".format(command))
        logger.exception(e, exc_info=True)
        client.send("Failed to execute command: {}".format(e))


def stop():
  if client:
    client.disconnect()
  server_sock.close()

  mqtt_disconnect()

  logger.info("Bluetooth server stopped.")
