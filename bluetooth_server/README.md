# Raspberry PI Bluetooth Server

_Control your PI with \` * Bluetooth magic *. * `_

## Installation

### Requirements

> Make sure you have at least Python 3.6 installed. If not, see this how-to: https://gist.github.com/dschep/24aa61672a2092246eaca2824400d37f (or also https://raspberrypi.stackexchange.com/questions/59381/how-do-i-update-my-rpi3-to-python-3-6)


For using bluetooth on the Raspberry, you need to have these packages installed:

```bash
# apt-get install bluetooth libbluetooth-dev
```

Install these python dependencies:

```bash
$ pip3 install pybluez    # For Bluetooth communication. You might want to install this one as sudo
```

> * pybluez: https://github.com/pybluez/pybluez

If any error occurs when running `systemctl status bluetooth.service` or the final `app.py` script, take a look at [bluetooth_problems.md](../docs/bluetooth_problems.md).


### Setup service

#### Bluetooth service

This service needs bluetooth to be enabled. It will continuously accept incoming connections.

Run `./setup_service.sh` to install the service (it will ask for your root password). After that, restart your system with `sudo reboot` or just use `systemctl start pi_bluetooth_server` to start the service.

##### Control

This service can be controlled by the `systemctl` command:

* `systemctl start pi_bluetooth_server`: Start the service
* `systemctl status pi_bluetooth_server`: Get the current status (logging) of the server
* `systemctl stop pi_bluetooth_server`: Stop the service
* `systemctl enable pi_bluetooth_server`: Start the service during boot
* `systemctl disable pi_bluetooth_server`: Stop starting the service during boot

##### Monitoring

Also, `journalctl` can be used for monitoring the logs:

```bash
$ journalctl -u pi_bluetooth_server.service
```

Add flags like `-e` for scrolling to the end or `-f` for live log following.

Complete logfile can be found in `/var/log/syslog`


## Run

```bash
$ python3 app.py
```

### Tests
I am a bad man and have made no tests :(

## Future

#### Nice to have's

* none yet