import logging

from bluetooth_server import server

logger = logging.getLogger(__name__)
logging.basicConfig(
  format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
  level=logging.DEBUG)


def run():
  server.start()
  try:
    while True:
      server.handle_connections()
  except KeyboardInterrupt:
    pass
  except Exception as e:
    logger.exception(e, exc_info=True)
  server.stop()


if __name__ == '__main__':
  run()
