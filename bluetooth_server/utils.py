

def get_additional_info(response_string: str = "Operation", duration: float = 0.0, timeout: float = 0.0):
  if duration > 0:
    if duration >= 120:
      response_string = "{} during {} minutes".format(response_string, round(duration / 60))
    else:
      response_string = "{} during {} seconds".format(response_string, duration)

  if timeout > 0:
    if timeout >= 120:
      response_string = "{} after {} minutes".format(response_string, round(timeout / 60))
    else:
      response_string = "{} after {} seconds".format(response_string, timeout)

  return response_string
