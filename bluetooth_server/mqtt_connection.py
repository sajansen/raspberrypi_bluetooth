import logging
import sys
import paho.mqtt.client as mqtt

from bluetooth_server import settings

logger = logging.getLogger(__name__)

mqtt_client: mqtt.Client = mqtt.Client()


def mqtt_connect():
    mqtt_client.on_connect = on_connect

    mqtt_client.username_pw_set(settings.MQTT_USERNAME, settings.MQTT_PASSWORD)
    mqtt_client.connect(settings.MQTT_HOST, settings.MQTT_PORT, 60)

    mqtt_client.loop_start()
    logger.info("MQTT connection created on {}".format(settings.MQTT_HOST))


def mqtt_disconnect():
    logger.info("Stopping MQTT connection")
    mqtt_client.loop_stop()
    mqtt_client.disconnect()


# The callback for when the mqtt_client receives a CONNACK response from the server.
def on_connect(client: mqtt.Client, userdata, flags, rc: int):
    if rc == mqtt.CONNACK_ACCEPTED:
        logger.info("Connected to MQTT broker")
    else:
        logger.warning("Connection result code ({}): {}".format(rc, mqtt.connack_string(rc)))
        raise ConnectionError("Failed to connect to MQTT broker: {}".format(mqtt.connack_string(rc)))
