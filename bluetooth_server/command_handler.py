import logging

from bluetooth_server.bluetooth_client import Client
from bluetooth_server.commands import system, server, mh, light

from common.command_handler import find_matching_command, Command

logger = logging.getLogger(__name__)


def handle_command(client: Client, text: str) -> None:
  command = find_matching_command(commands, text)

  if command is None:
    client.send("Unknown command. Try 'help' for a list of available commands.")
    return

  command.execute(text, client)


commands = [
  # Server
  Command(r"^(help|\?)", server.send_help),
  Command(r"^rssi$", server.get_and_send_rssi),
  Command(r"^hello$", server.hello),
  Command(r"^disconnect$", server.disconnect),

  # Service
  Command(r"^service update$", system.update_service),
  Command(r"^service restart ?(?P<service>light|bluetooth)?$", system.restart_services),

  Command(r"^mqtt status$", system.mqtt_status),

  # System
  Command(r"^pi reboot$", system.reboot_pi),
  Command(r"^pi shutdown$", system.shutdown_pi),
  Command(r"^pi stop music$", system.stop_music),
  Command(r"^pi run <any:command>$", system.run_command),

  # Light
  Command(r"^light (brightness|level|set) <int:value> ?<float:duration>? ?<float:timeout>?$", light.brightness),
  Command(r"^desk brightness <int:value> ?<float:duration>? ?<float:timeout>?$", light.brightness),

  # MovingHead
  Command(r"^mh on ?<float:duration>? ?<float:timeout>?$", mh.on),
  Command(r"^mh off ?<float:duration>? ?<float:timeout>?$", mh.off),
  Command(r"^mh (brightness|level) <int:value> ?<float:duration>? ?<float:timeout>?$", mh.brightness),
  Command(r"^mh color <int:r> <int:g> <int:b> ?<float:duration>? ?<float:timeout>?$", mh.color),
  Command(r"^mh effect speed <int:value> ?<float:duration>? ?<float:timeout>?$", mh.effect_speed),
  Command(r"^mh effect <str:name> ?<float:timeout>?$", mh.effect),
  Command(r"^mh pitch <int:value> ?<float:duration>? ?<float:timeout>?$", mh.pitch),
  Command(r"^mh rotation <int:value> ?<float:duration>? ?<float:timeout>?$", mh.rotation),
]
