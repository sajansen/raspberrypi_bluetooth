#!/bin/bash

# Prepare service file
echo "Creating service"
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
PARENTPATH="$(dirname ${SCRIPTPATH})"
cat "${SCRIPTPATH}/pi_bluetooth_server.service.template" | sed "s#\/directorypath#${PARENTPATH}#g" > pi_bluetooth_server.service

# Deploy service file
echo "Deploying service"
sudo cp pi_bluetooth_server.service /etc/systemd/system/
sudo chmod 664 /etc/systemd/system/pi_bluetooth_server.service

# Clean up our garbage
echo "Cleaning up"
rm pi_bluetooth_server.service

# Run service file
echo "Enabling service at boot"
sudo systemctl enable pi_bluetooth_server   # Run at boot

echo "Done :)"