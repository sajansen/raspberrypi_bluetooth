import unittest

from bluetooth_server import settings
from bluetooth_server.bluetooth_client import Client
from bluetooth_server.commands import mh


class UtilsTest(unittest.TestCase):

  def setUp(self) -> None:
    mh.mh_client = MHClientMock()

  def test_set_brightness(self):
    def assert_set_brightness(value: int = 0, duration: float = 0.0, timeout: float = 0.0):
      self.assertEqual(100, value)
      self.assertEqual(2.0, duration)
      self.assertEqual(1.5, timeout)

    mh.mh_client.set_brightness = assert_set_brightness
    client = BluetoohClientMock()

    mh.brightness(client, value="100", duration="2.0", timeout="1.5")

    self.assertEqual(["Setting brightness to 100 during 2.0 seconds after 1.5 seconds"], client.messages_send)

  def test_set_brightness_with_zero_timeout_value(self):
    def assert_set_brightness(value: int = 0, duration: float = 0.0, timeout: float = 0.0):
      self.assertEqual(100, value)
      self.assertEqual(2.0, duration)
      self.assertEqual(0.0, timeout)

    mh.mh_client.set_brightness = assert_set_brightness
    client = BluetoohClientMock()

    mh.brightness(client, value="100", duration="2.0", timeout="0.0")

    self.assertEqual(["Setting brightness to 100 during 2.0 seconds"], client.messages_send)

  def test_set_brightness_with_none_timeout_value(self):
    def assert_set_brightness(value: int = 0, duration: float = 0.0, timeout: float = 0.0):
      self.assertEqual(100, value)
      self.assertEqual(2.0, duration)
      self.assertEqual(0.0, timeout)

    mh.mh_client.set_brightness = assert_set_brightness
    client = BluetoohClientMock()

    mh.brightness(client, value="100", duration="2.0")

    self.assertEqual(["Setting brightness to 100 during 2.0 seconds"], client.messages_send)


class BluetoohClientMock(Client):
  def __init__(self):
    self.messages_send = []

  def clear_messages(self):
    self.messages_send.clear()

  def send(self, message: str, end=settings.MESSAGE_TERMINATOR):
    self.messages_send.append(message)

  def disconnect(self):
    pass


class MHClientMock:
  def set_brightness(self, value: int = 0, duration: float = 0.0, timeout: float = 0.0):
    pass
