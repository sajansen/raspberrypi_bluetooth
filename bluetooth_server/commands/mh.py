import logging

from bluetooth_server.bluetooth_client import Client
from bluetooth_server.mqtt_connection import mqtt_client
from bluetooth_server.utils import get_additional_info
from light_controller.movinghead import client as mh_client

logger = logging.getLogger(__name__)


def on(client: Client, duration: str = "0.0", timeout: str = "0.0"):
  duration = float(duration)
  timeout = float(timeout)

  response_string = "Turning MovingHead on"
  response_string = get_additional_info(response_string, duration, timeout)
  client.send(response_string)

  mh_client.set_on(mqtt_client, duration, timeout)


def off(client: Client, duration: str = "0.0", timeout: str = "0.0"):
  duration = float(duration)
  timeout = float(timeout)

  response_string = "Turning MovingHead off"
  response_string = get_additional_info(response_string, duration, timeout)
  client.send(response_string)

  mh_client.set_off(mqtt_client, duration, timeout)


def brightness(client: Client, value: str, duration: str = "0.0", timeout: str = "0.0"):
  duration = float(duration)
  timeout = float(timeout)

  response_string = "Setting brightness to {}".format(value)
  response_string = get_additional_info(response_string, duration, timeout)
  client.send(response_string)

  mh_client.set_brightness(mqtt_client, int(value), duration, timeout)


def color(client: Client, r: str, g: str, b: str, duration: str = "0.0", timeout: str = "0.0"):
  duration = float(duration)
  timeout = float(timeout)

  response_string = "Setting color to ({}, {}, {})".format(r, g, b)
  response_string = get_additional_info(response_string, duration, timeout)
  client.send(response_string)

  mh_client.set_color(mqtt_client, int(r), int(g), int(b), duration, timeout)


def effect(client: Client, name: str, timeout: str = "0.0"):
  timeout = float(timeout)

  response_string = "Setting effect to {}".format(name)
  response_string = get_additional_info(response_string, timeout=timeout)
  client.send(response_string)

  mh_client.set_effect(mqtt_client, name.upper(), float(timeout))


def effect_speed(client: Client, value: str, duration: str = "0.0", timeout: str = "0.0"):
  duration = float(duration)
  timeout = float(timeout)

  response_string = "Setting effect speed to {}".format(value)
  response_string = get_additional_info(response_string, duration, timeout)
  client.send(response_string)

  mh_client.set_effect_speed(mqtt_client, int(value), duration, timeout)


def pitch(client: Client, value: str, duration: str = "0.0", timeout: str = "0.0"):
  duration = float(duration)
  timeout = float(timeout)

  response_string = "Setting pitch to {}".format(value)
  response_string = get_additional_info(response_string, duration, timeout)
  client.send(response_string)

  mh_client.set_pitch(mqtt_client, int(value), duration, timeout)


def rotation(client: Client, value: str, duration: str = "0.0", timeout: str = "0.0"):
  duration = float(duration)
  timeout = float(timeout)

  response_string = "Setting rotation to {}".format(value)
  response_string = get_additional_info(response_string, duration, timeout)
  client.send(response_string)

  mh_client.set_rotation(mqtt_client, int(value), duration, timeout)
