import logging
import os
import shlex
import subprocess
import sys
from typing import Optional

from bluetooth_server import settings
from bluetooth_server.bluetooth_client import Client
from bluetooth_server.mqtt_connection import mqtt_client
from light_controller import settings as light_controller_settings

logger = logging.getLogger(__name__)


def update_service(client: Client) -> None:
    logger.info("Updating service")
    client.send("Updating service...")

    parent_directory = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    command = "su - pi -c 'cd {} && git pull'".format(parent_directory)

    logger.debug("Executing command: {}".format(command))
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if output:
        logger.info(output.decode())
    if error:
        logger.error(error.decode())
        client.send("Error updating service: ")
        client.send(error.decode())
        return error.decode()

    if "Already up to date :)" in output.decode():
        return
    else:
        client.send(output.decode())

    restart_light_controller_service()
    restart_bluetooth_service()


def restart_service(service: str) -> Optional[str]:
    logger.info("Restarting service: {}".format(service))
    command = "systemctl restart {}".format(service)
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    output, error = process.communicate()
    if output:
        logger.info(output.decode())
    if error:
        logger.error(error.decode())
        return error.decode()
    return None


def restart_bluetooth_service() -> Optional[str]:
    return restart_service(settings.SERVICE_NAME)


def restart_light_controller_service() -> Optional[str]:
    return restart_service(light_controller_settings.SERVICE_NAME)


def restart_services(client: Client, service=None):
    if service is None or service == "light":
        client.send("Restarting light service... ", end="")
        error = restart_light_controller_service()
        if error:
            client.send("error:")
            client.send(error)
        else:
            client.send("done")

    if service is None or service == "bluetooth":
        client.send("Restarting bluetooth service... ", end="")
        error = restart_bluetooth_service()
        if error:
            client.send("error:")
            client.send(error)
        else:
            client.send("done")


def restart_bluetooth() -> None:
    logger.info("Restarting service: bluetooth.service")

    command = "systemctl restart bluetooth.service"
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if output:
        logger.info(output.decode())
    if error:
        logger.error(error.decode())

    # Wait for bluetooth service to restart and then restart self
    import time
    time.sleep(2)
    restart_bluetooth_service()


def reboot_pi(client: Client) -> None:
    logger.info("Rebooting pi")
    client.send("Rebooting PI...")

    command = "reboot"
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if output:
        logger.info(output.decode())
    if error:
        logger.error(error.decode())

    sys.exit(0)


def shutdown_pi(client: Client) -> None:
    logger.info("Shutting down pi")
    client.send("Shutting down PI...")

    command = "shutdown -h now"
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if output:
        logger.info(output.decode())
    if error:
        logger.error(error.decode())

    sys.exit(0)


def stop_music(client: Client) -> None:
    logger.info("Killing omxplayer")
    client.send("Stopping the music... (sorry Rihanna) ", end="")

    command = "killall -s 9 omxplayer; killall -s 9 omxplayer.bin"
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if output:
        logger.info(output.decode())
    if error:
        client.send("error:")
        client.send(error)
        logger.error(error.decode())

    client.send("done")


def run_command(client: Client, command: str) -> None:
    logger.info("Running command: {}".format(command))
    client.send("Running command: {}".format(command))

    try:
        process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
        output, error = process.communicate()

        if output:
            logger.info(output.decode())
            client.send(output.decode())

        if error:
            logger.error(error.decode())
            client.send("Command returned an error: ")
            client.send(error.decode())
        else:
            client.send("\nDone running command.")

    except Exception as e:
        logger.error("Failed to run the given command")
        logger.exception(e, exc_info=True)
        client.send("Failed to run the given command: {}".format(e))


def mqtt_status(client: Client) -> None:
    logger.info("Getting MQTT status")
    client.send("MQTT status: {} to {}@{}".format("connected" if mqtt_client.is_connected() else "disconnected",
                                                  settings.MQTT_USERNAME, settings.MQTT_HOST))
