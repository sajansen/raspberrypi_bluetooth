import logging

from bluetooth_server.bluetooth_client import Client
from bluetooth_server.mqtt_connection import mqtt_client
from bluetooth_server.utils import get_additional_info
from light_controller.desk_led import client as light_client

logger = logging.getLogger(__name__)


def brightness(client: Client, value: str, duration: str = 0.0, timeout: float = 0.0):
  duration = float(duration)
  timeout = float(timeout)

  response_string = "Setting brightness to {}".format(value)
  response_string = get_additional_info(response_string, duration, timeout)
  client.send(response_string)

  light_client.set_brightness(mqtt_client, int(value), duration, timeout)
