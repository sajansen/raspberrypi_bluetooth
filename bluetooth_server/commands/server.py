import logging

from bluetooth_server import settings
from bluetooth_server.bluetooth_client import Client

logger = logging.getLogger(__name__)


def get_and_send_rssi(client: Client):
  rssi = client.rssi()
  logger.info("Client rssi: {}".format(rssi))

  if rssi:
    client.send("RSSI: " + rssi)
    return

  client.send("Failed to obtain RSSI")


def send_help(client: Client):
  client.send(settings.USAGE)


def hello(client: Client):
  logger.info("Responding to 'hello'")
  client.send("Hi there")


def disconnect(client: Client):
  logger.info("Disconnecting client")
  client.send("Bye")
  client.disconnect()
