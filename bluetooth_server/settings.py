# System
SERVICE_NAME = "pi_bluetooth_server"  # Name of this server running as service

# MQTT
MQTT_TOPIC = "light_controller"
MQTT_HOST = "localhost"
MQTT_PORT = 1883
MQTT_USERNAME = "admin"
MQTT_PASSWORD = "admin"

# Bluetooth server
SERVER_NAME = "Pi Bluetooth Server"
SERVER_UUID = "ca15f74a-0934-11ea-8d71-362b9e155667"
MESSAGE_TERMINATOR = "\r\n"  # Terminate send messages with this default terminator
USAGE = """Light Controller with Bluetooth Server
Commands:
  help, ?: Show this message
  disconnect: Disconnect as client
  service restart [bluetooth|light]: Restart the specified or all services (if none specified)
  service update: Update the service from git and restarts them
  mqtt status: Get the status of MQTT connection
  pi reboot: Reboot the device
  pi shutdown: Shutdown the device
  pi stop music: Stops any music being played, by killing omxplayer
  pi run <command>: Runs the given command in the PI terminal
  hello: Will reply with 'hi there'
  rssi: Get the RSSI of the client relative to the server
  light (brightness|level|set) <value> [duration] [timeout]: Fade in/out the light for a certain duration (seconds, default: 0) to a certain brightness (0-8192)
  desk brightness <value> [duration] [timeout]: Same as command 'light'
  mh <on|off>: Set the MovingHead on or off
  mh <mode> <value>: Set the value for a certain part of the MovingHead. Available modes: brightness/level, effect, effect speed, rotation, pitch . Available effects: OFF, SOLID, STROBE, STARS, SMILEY, RAINBOW
  mh color <red> <green> <blue>: Set the color of the MovingHead 

"""
