import logging
import shlex
import subprocess

from bluetooth_server import settings

logger = logging.getLogger(__name__)
logging.basicConfig(
  format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
  level=logging.DEBUG)


class Client:
  def __init__(self, client_sock, client_info):
    self.sock = client_sock
    self.info = client_info
    self.mac = self.info[0]

  def send(self, message: str, end=settings.MESSAGE_TERMINATOR):
    logger.info("Sending: {}".format(message))
    self.sock.sendall(message + end)

  def disconnect(self):
    logger.info("Disconnecting client")
    self.sock.close()

  def rssi(self):
    command = "hcitool rssi {}".format(self.mac)
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    output, error = process.communicate()

    if output is None:
      logger.error(error.decode())
      return None

    output_array = output.decode().split()
    if len(output_array) == 4:
      return output_array[3]

    logger.error("Couldn't get RSSI value")
    if output:
      logger.error(output.decode())
    if error:
      logger.error(error.decode())
    return None
