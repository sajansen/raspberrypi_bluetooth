import unittest

from bluetooth_server import utils


class UtilsTest(unittest.TestCase):

  def test_get_additional_info_with_seconds(self):
    self.assertEqual("start during 2.2 seconds after 3.0 seconds", utils.get_additional_info("start", 2.2, 3.0))
    self.assertEqual("start during 2.2 seconds", utils.get_additional_info("start", 2.2))
    self.assertEqual("start during 2.2 seconds", utils.get_additional_info("start", duration=2.2))
    self.assertEqual("start after 3 seconds", utils.get_additional_info("start", timeout=3))
    self.assertEqual("start after 3 seconds", utils.get_additional_info("start", 0, 3))
    self.assertEqual("Operation during 2.2 seconds after 3 seconds", utils.get_additional_info(duration=2.2, timeout=3))

  def test_get_additional_info_with_minutes(self):
    self.assertEqual("start during 119.0 seconds after 119.0 seconds", utils.get_additional_info("start", 119.0, 119.0))
    self.assertEqual("start during 2 minutes after 3 minutes", utils.get_additional_info("start", 120.0, 180.0))
    self.assertEqual("start during 2 minutes after 3 minutes", utils.get_additional_info("start", 121.0, 181.0))
    self.assertEqual("start during 2 minutes after 3 minutes", utils.get_additional_info("start", 149.9, 209.9))
    self.assertEqual("start during 3 minutes after 4 minutes", utils.get_additional_info("start", 151, 211))
    self.assertEqual("start during 3 minutes after 4 minutes", utils.get_additional_info("start", 179, 239))
    self.assertEqual("start during 2 minutes", utils.get_additional_info("start", 120))
    self.assertEqual("start during 2 minutes", utils.get_additional_info("start", duration=120))
    self.assertEqual("start after 3 minutes", utils.get_additional_info("start", timeout=180))
    self.assertEqual("start after 3 minutes", utils.get_additional_info("start", 0, 180))
    self.assertEqual("Operation during 2 minutes after 3 minutes", utils.get_additional_info(duration=120, timeout=180))
