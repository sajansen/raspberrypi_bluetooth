#!/bin/bash
#Purpose = Execute health check(s)
# crontab -e
# */5 * * * * /workingdirectory/main.sh

URLS=(
"https://sajansen.nl"
"https://jschedule.sajansen.nl/api/v1/health"
"https://rehobothkerkwoerden.nl/api/v1/"
)

function onFail() {
  URL="$1"
  ERROR="$2"
  echo "FAIL: ${ERROR}"
  rabbitmqadmin publish exchange=amq.topic routing_key="errors" payload="{\"service\": \"healthcheck_service\", \"message\": \"Healthcheck failed with error '${ERROR}' for: ${URL}\"}"
}

function main() {
  for URL in "${URLS[@]}"; do
    echo -n "[main] => Checking: ${URL}... "
    output=$(curl -sf --connect-timeout 60 --max-time 300 --retry 5 --retry-delay 0 --retry-max-time 40 "${URL}")
    result=$?

    # Check if curl succeeded
    if [ 0 -ne $result ]; then
      onFail "$URL" "curl error ${result}"
      continue
    fi

    # Check if output is empty
    if [ -z "${output}" ]; then
      onFail "$URL" "empty response"
      continue
    fi

    echo "OK"
  done
}

main
