import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Database
db_uri = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
db_query_logging = False

# MQTT
MQTT_TOPIC = "light_controller"
MQTT_HOST = "localhost"
MQTT_PORT = 1883
MQTT_USERNAME = "admin"
MQTT_PASSWORD = "admin"

# ADS1115
ads1115_address = 0x48
ads1115_temp_sensor_channel = 0
ads1115_light_sensor_channel = 3
# For each average reading, take this amount of samples
ads1115_average_measurements_count = 4

## TEMPERATURE SENSOR PROPERTIES
# Reading voltage offset (V)
temp_sensor_offset_voltage = 0.5
# Voltage scaling (after offset) (V/°C)
temp_sensor_voltage_scaling = 0.010

# Round temperature when printing to console
temp_rounding_decimals = 2

## LIGHT SENSOR PROPERTIES
# Input voltage (V) feeding the sensor circuit
input_voltage = 3.3
# Resistance (ohm) put into series with the LDR
r_1_resistance = 4_700

localsettingspath = os.path.join(BASE_DIR, "settings_local.py")
if os.path.exists(localsettingspath):
    from analog_sensors_service.settings_local import *
