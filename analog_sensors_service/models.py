from sqlalchemy import Column, Integer, DateTime, Float

from analog_sensors_service.db import Base


class TemperatureReading(Base):
    __tablename__ = 'temp_readings'

    id = Column(Integer, primary_key=True)
    temperature = Column(Float, nullable=False)
    created_at = Column(DateTime, nullable=False)

    def __repr__(self):
        return "TempReading(id={}, temperature={}, created_at={})" \
            .format(self.id, self.temperature, self.created_at)


class LightReading(Base):
    __tablename__ = 'light_readings'

    id = Column(Integer, primary_key=True)
    raw_value = Column(Float, nullable=False)
    resistance = Column(Float, nullable=False)
    created_at = Column(DateTime, nullable=False)

    def __repr__(self):
        return "LightReading(id={}, raw_value={}, resistance={}, created_at={})" \
            .format(self.id, self.raw_value, self.resistance, self.created_at)
