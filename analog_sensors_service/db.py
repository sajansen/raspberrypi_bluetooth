import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, declarative_base

from analog_sensors_service import settings

logger = logging.getLogger(__name__)

Base = declarative_base()

_engine = None


def connect():
    global _engine
    logger.info("Connecting to database: {}".format(settings.db_uri))
    _engine = create_engine(settings.db_uri, echo=settings.db_query_logging, future=True)

    Base.metadata.create_all(_engine)


def session():
    return Session(_engine)
