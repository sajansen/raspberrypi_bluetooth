import logging

from analog_sensors_service import settings
from common.i2c import i2c
from common.timings import sleep_ms

logger = logging.getLogger(__name__)

_i2c_queue = i2c.queue


class ADS1115:
    # CONVERSION DELAY (in mS)
    ADS1115_CONVERSIONDELAY = 8

    # POINTER REGISTER
    REG_POINTER_MASK = 0x03
    REG_POINTER_CONVERT = 0x00
    REG_POINTER_CONFIG = 0x01
    REG_POINTER_LOWTHRESH = 0x02
    REG_POINTER_HITHRESH = 0x03

    # CONFIG REGISTER
    REG_CONFIG_OS_MASK = 0x8000
    REG_CONFIG_OS_SINGLE = 0x8000  # Write: Set to start a single-conversion
    REG_CONFIG_OS_BUSY = 0x0000  # Read: Bit = 0 when conversion is in progress
    REG_CONFIG_OS_NOTBUSY = 0x8000  # Read: Bit = 1 when device is not performing a conversion

    REG_CONFIG_MUX_MASK = 0x7000
    REG_CONFIG_MUX_SINGLE_0 = 0x4000  # Single - ended AIN0
    REG_CONFIG_MUX_SINGLE_1 = 0x5000  # Single - ended AIN1
    REG_CONFIG_MUX_SINGLE_2 = 0x6000  # Single - ended AIN2
    REG_CONFIG_MUX_SINGLE_3 = 0x7000  # Single - ended AIN3

    REG_CONFIG_PGA_MASK = 0x0e00
    REG_CONFIG_PGA_6_144V = 0x0000  # +/-6.144V range = Gain 2/3
    REG_CONFIG_PGA_4_096V = 0x0200  # +/-4.096V range = Gain 1
    REG_CONFIG_PGA_2_048V = 0x0400  # +/-2.048V range = Gain 2 (default)
    REG_CONFIG_PGA_1_024V = 0x0600  # +/-1.024V range = Gain 4
    REG_CONFIG_PGA_0_512V = 0x0800  # +/-0.512V range = Gain 8
    REG_CONFIG_PGA_0_256V = 0x0A00  # +/-0.256V range = Gain 16

    REG_CONFIG_MODE_MASK = 0x0100
    REG_CONFIG_MODE_CONTIN = 0x0000  # Continuous conversion mode
    REG_CONFIG_MODE_SINGLE = 0x0100  # Single-shot mode (default)

    REG_CONFIG_DR_MASK = 0x00E0
    REG_CONFIG_DR_128SPS = 0x0000  # 128 samples per second
    REG_CONFIG_DR_250SPS = 0x0020  # 250 samples per second
    REG_CONFIG_DR_490SPS = 0x0040  # 490 samples per second
    REG_CONFIG_DR_920SPS = 0x0060  # 920 samples per second
    REG_CONFIG_DR_1600SPS = 0x0080  # 1600 samples per second (default)
    REG_CONFIG_DR_2400SPS = 0x00A0  # 2400 samples per second
    REG_CONFIG_DR_3300SPS = 0x00C0  # 3300 samples per second

    REG_CONFIG_CMODE_TRAD = 0x00  # Traditional comparator (default val)

    REG_CONFIG_CPOL_ACTVLOW = 0x00  # Alert/Rdy active low   (default val)

    REG_CONFIG_CLAT_NONLAT = 0x00  # Non-latching (default val)

    REG_CONFIG_CQUE_NONE = 0x03  # Disable the comparator (default val)

    def __init__(self, address: int = 0x48):
        self.address = address
        self.gain = self.REG_CONFIG_PGA_2_048V
        self.conversion_delay = max(self.ADS1115_CONVERSIONDELAY, 500)

    def setup(self):
        i2c.start()

    def _generate_config(self, channel):
        config = self.REG_CONFIG_CQUE_NONE | \
                 self.REG_CONFIG_CLAT_NONLAT | \
                 self.REG_CONFIG_CPOL_ACTVLOW | \
                 self.REG_CONFIG_CMODE_TRAD | \
                 self.REG_CONFIG_DR_128SPS | \
                 self.REG_CONFIG_MODE_SINGLE

        config |= self.gain

        if channel == 0:
            config |= self.REG_CONFIG_MUX_SINGLE_0
        if channel == 1:
            config |= self.REG_CONFIG_MUX_SINGLE_1
        if channel == 2:
            config |= self.REG_CONFIG_MUX_SINGLE_2
        if channel == 3:
            config |= self.REG_CONFIG_MUX_SINGLE_3

        config |= self.REG_CONFIG_OS_SINGLE

        return config

    def _get_max_voltage(self) -> float:
        if self.gain == self.REG_CONFIG_PGA_6_144V:
            return 6.144
        if self.gain == self.REG_CONFIG_PGA_4_096V:
            return 4.096
        if self.gain == self.REG_CONFIG_PGA_2_048V:
            return 2.048
        if self.gain == self.REG_CONFIG_PGA_1_024V:
            return 1.024
        if self.gain == self.REG_CONFIG_PGA_0_512V:
            return 0.512
        if self.gain == self.REG_CONFIG_PGA_0_256V:
            return 0.256
        return 2.048  # default

    def read_average(self, channel: int, measurements: int = settings.ads1115_average_measurements_count) -> float:
        if measurements <= 0:
            measurements = 1

        readings = []
        for i in range(0, measurements):
            readings.append(self.read(channel))

        logger.debug("Taking average of: {}".format(readings))
        return sum(readings) / measurements

    def read(self, channel: int) -> float:
        if channel > 3:
            channel = 0

        config = self._generate_config(channel)

        i2c.send(_i2c_queue, self.address, self.REG_POINTER_CONFIG, [
            config >> 8,
            config & 0xff,
        ])

        sleep_ms(self.conversion_delay)

        # Set register to read from
        result_list = i2c.read(self.address, self.REG_POINTER_CONVERT, length=2)
        result = (result_list[0] << 8) + result_list[1]

        # Check for two's complement
        if (result >> 15) == 1:
            result = result - 1 - 0xffff

        factor = result / 0x7fff
        voltage = factor * self._get_max_voltage()

        return voltage
