import logging
import sys
from datetime import datetime

from analog_sensors_service import db, settings
from analog_sensors_service.ADS1115 import ADS1115
from analog_sensors_service.models import TemperatureReading, LightReading
from common.mqtt.mqtt import MQTT

logger = logging.getLogger(__name__)
logging.basicConfig(
    stream=sys.stdout,
    format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
    level=logging.DEBUG)

sensor_reader = ADS1115(settings.ads1115_address)

mqtt = MQTT()


def report_error(message: str):
    logger.warning("Reporting error: {}".format(message))
    mqtt.try_send("errors", "{{\"service\": \"{}\", \"message\": \"{}\"}}".format("analog_sensors_service", message))


def read_only_channel(channel: int):
    sensor_reader.gain = ADS1115.REG_CONFIG_PGA_6_144V
    raw_voltage = sensor_reader.read_average(channel)
    logger.debug("Voltage from sensor: {}".format(raw_voltage))


def take_temperature_reading() -> TemperatureReading:
    logger.info("Reading analog sensor value")

    sensor_reader.gain = ADS1115.REG_CONFIG_PGA_2_048V
    raw_voltage = sensor_reader.read_average(settings.ads1115_temp_sensor_channel)
    logger.debug("Voltage from sensor: {}".format(raw_voltage))

    # Convert reading to temperature
    unbiased_voltage = raw_voltage - settings.temp_sensor_offset_voltage
    converted_voltage = unbiased_voltage / settings.temp_sensor_voltage_scaling

    reading = TemperatureReading(temperature=converted_voltage, created_at=datetime.now())

    return reading


def take_light_reading() -> LightReading:
    logger.info("Reading analog sensor value")

    sensor_reader.gain = ADS1115.REG_CONFIG_PGA_6_144V
    raw_voltage = sensor_reader.read_average(settings.ads1115_light_sensor_channel)
    logger.debug("Voltage from sensor: {}".format(raw_voltage))

    ##
    #   Given:
    #                    +-- V_sens --+
    #                    |            |
    #  V_in  ---  R_1  --+-- R_ldr  --+--  GND
    #
    #   Where:
    #
    #  V_sens = V_in * R_ldr / ( R_1 + R_ldr )
    #
    #   Thus:
    #
    #  R_ldr = R_1 * V_sens / ( V_in + V_sens)
    #
    ##
    resistance = settings.r_1_resistance * raw_voltage / (settings.input_voltage - raw_voltage)

    reading = LightReading(raw_value=raw_voltage, resistance=resistance, created_at=datetime.now())

    return reading


def save_reading(reading):
    logger.info("Saving new reading to database")
    with db.session() as session:
        session.expire_on_commit = False
        session.add(reading)
        session.commit()


def print_all_temperature_readings():
    logger.info("Getting all temperature readings from database")
    with db.session() as session:
        results = session.query(TemperatureReading).order_by(TemperatureReading.created_at.desc())
        for result in results:
            print("On {} it was {} °C"
                  .format(result.created_at.strftime("%d %B %Y at %H:%M:%S"),
                          round(result.temperature, settings.temp_rounding_decimals)))


def print_all_light_readings():
    logger.info("Getting all light readings from database")
    with db.session() as session:
        results = session.query(LightReading).order_by(LightReading.created_at.desc())
        for result in results:
            print("On {} the sensor measured {} Ω of resistance"
                  .format(result.created_at.strftime("%d %B %Y at %H:%M:%S"),
                          round(result.resistance)))


def read_only_temperature_readings():
    print("Reading (read only): {}".format(take_temperature_reading()))


def read_only_light_readings():
    print("Reading (read only): {}".format(take_light_reading()))


def take_and_save_temperature():
    reading = take_temperature_reading()
    logger.info("Got new temperature reading: {}".format(reading))
    save_reading(reading)

    if reading.temperature < -35:
        report_error("Got invalid temperature reading. Is the sensor disconnected? Reading: {}"
                     .format(reading.temperature))


def take_and_save_light():
    reading = take_light_reading()
    logger.info("Got new light reading: {}".format(reading))
    save_reading(reading)


def run(args=None) -> None:
    if args is None:
        args = []

    if "--help" in args:
        return print("""
        python app.py [args]
            Reads values from analog sensors and stores them into the database
            
        --help              Only show this message
        --list-temperature  Only get and print temperature data from database
        --list-light        Only get and print light data from database
        --read-temperature  Only read and print temperature data without inserting it into the database
        --read-light        Only read and print light data without inserting it into the database
        """)

    db.connect()

    if "--list-temperature" in args:
        return print_all_temperature_readings()
    if "--list-light" in args:
        return print_all_light_readings()
    if "--read-temperature" in args:
        return read_only_temperature_readings()
    if "--read-light" in args:
        return read_only_light_readings()
    if "--read-channel" in args:
        return read_only_channel(int(args[1]) if len(args) > 1 else 0)
    if len(args) > 0:
        print("Unknown command(s): {}. Try --help for a list of available commands.".format(' '.join(args)))
        return

    mqtt.try_connect(max_delay=10)

    sensor_reader.setup()

    take_and_save_temperature()
    take_and_save_light()

    mqtt.disconnect()


if __name__ == "__main__":
    run(sys.argv)
