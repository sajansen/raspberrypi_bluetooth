# Analog sensors

This module takes measurements of analog sensors and stores it into a database. Examples are: temperature and light sensors.

## Setup

The analog sensor must be connected to one of the four channels of a ADS1115 analog reader. Specify this channel in the settings.

This reader must be connected to the Raspberry PI through I2C. Specify its address in the settings.

Set the correct sensor properties in the settings, like bias and conversion scaling or resistance properties. 

### Temperature

The default properties are from this sensor: TMP36 ([datasheet](https://www.analog.com/media/en/technical-documentation/data-sheets/TMP35_36_37.pdf#%5B%7B%22num%22%3A15%2C%22gen%22%3A0%7D%2C%7B%22name%22%3A%22XYZ%22%7D%2C36%2C714%2Cnull%5D)).

### Light

The default properties are for a common LDR sensor to be placed in series with a 4.7 kOhm resistor. The voltage across the LDR is measured, which is connected to ground. See the following schematic.

```
                   +-- V_sens --+
                   |            |
 V_in  ---  R_1  --+-- R_ldr  --+--  GND
```

## Run

Let the service run in intervals as desired. Running it with a cronjob can be recommended. 

