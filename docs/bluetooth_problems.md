
When you check the status of the bluetooth service and errors occurs, I have found a few solutions for them. Most of the commands need to be run as sudo.

## `systemctl status bluetooth.service`

When you see the following error, go to (1):

```bash
   Sap driver initialization failed.
   sap-server: Operation not permitted (1)
   Failed to set mode: Blocked through rfkill (0x12)
```

When you see the following error, go to (2):

```bash
   Failed to set mode: Blocked through rfkill (0x12)
```

When you see the following error, go to (3):

```bash
   Failed to set privacy: Rejected (0x0b)
```
 

#### (1) sap-server: Operation not permitted

The service needs to be restarted with SAP disbled. Edit the service file:

```bash
vim /etc/systemd/system/bluetooth.target.wants/bluetooth.service
```

and change this line:

```bash
  ExecStart=/usr/lib/bluetooth/bluetoothd
```

to:

```bash
  ExecStart=/usr/lib/bluetooth/bluetoothd --noplugin=sap -C
```

Note: the last extra parameter `-C` is actually not for solving this problem, but to prevent the problem from (4) from happening.

Finish it with reloading the services and checking the status of the service again:

```bash
systemctl daemon-reload
service bluetooth restart
systemctl status bluetooth.service
```


#### (2) Blocked through rfkill

We need to unblock the bluetooth device. 

Get the ID number of the bluetooth device from the list of services:

```bash
rfkill list
```

Use this ID to unblock the bluetooth device:

```bash
rfkill unblock <ID>
```

> If this doesn't unblock it, use this:
> ```bash
> rfkill unblock all
> ```

Finish it with reloading the services and checking the status of the service again:

```bash
sudo service bluetooth restart
systemctl status bluetooth.service
```


#### (3) Failed to set privacy: Rejected (0x0b)

This may happen on system startup. Just restart the service:

```bash
sudo systemctl restart bluetooth.service
```

It should work normal now:

```bash
systemctl status bluetooth.service
```


## `python3 app.py`

When you see the following error, go to (4):

```bash
   _bluetooth.error: (2, 'No such file or directory')
```


#### (4) _bluetooth.error: (2, 'No such file or directory')

You will get this error when running the python script to start the server (`bluetooth.advertise_service()`).

The hardware needs to be added:

```bash
sudo sdptool add SP
```

If this fails, go to (1)

Finally, just restart the system to be sure it will work.

```bash
sudo reboot
```
