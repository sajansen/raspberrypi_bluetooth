import base64
import binascii
import datetime
import importlib
import logging
import os
import sys
import time
from typing import Optional, List, Tuple

import light_controller
from common.mqtt.mqtt import MQTT
from light_controller.desk_led import client as light_client
from light_controller.movinghead import client as mh_client
from wifi_scanner import settings, db
from wifi_scanner.models import Connection
from wifi_scanner.modems.ConnectboxGiga import ConnectboxGiga
from wifi_scanner.modems.ConnextboxCompal import ConnextboxCompal
from wifi_scanner.modems.ZTE_MF286C import ZTE_MF286C
from wifi_scanner.modems.modem import Modem
from wifi_scanner.objects import Device, ModemDevice
from wifi_scanner.state import save_state
from sqlalchemy.orm import Session

logger = logging.getLogger(__name__)
logging.basicConfig(
    stream=sys.stdout,
    format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
    level=logging.DEBUG)

mqtt = MQTT(host=settings.MQTT_HOST,
            port=settings.MQTT_PORT,
            username=settings.MQTT_USERNAME,
            password=settings.MQTT_PASSWORD)


def report_error(message: str):
    logger.warning("Reporting error: {}".format(message))
    mqtt.try_send("errors", "{{\"service\": \"{}\", \"message\": \"{}\"}}".format("wifi_scanner", message))


def lights_on() -> None:
    if not settings.enable_lights_on:
        return

    logger.info("Lights on")

    # Send commands...
    logger.info("  sending MQTT commands...")
    light_client.set_brightness(mqtt.client(), int(light_controller.settings.MAX_BRIGHTNESS), 5.0)
    mh_client.set_on(mqtt.client(), 5.0)


def lights_off() -> None:
    if not settings.enable_lights_off:
        return

    logger.info("Lights off")

    # Send commands...
    logger.info("  sending MQTT commands...")
    light_client.set_brightness(mqtt.client(), 0, 5.0)
    mh_client.set_off(mqtt.client(), 5.0)


def reboot_modem_if_no_internet_connection(modem: Modem):
    counter = 5
    while counter > 0:
        if modem.check_internet_connection():
            return

        counter -= 1
        time.sleep(10)
    modem.reboot()


def run() -> None:
    mqtt.try_connect(log_exception=False, max_delay=10)

    # Get devices info from router
    modem = get_modem()
    if modem is None:
        logger.error("No valid router specified: %s", settings.router)
        return

    if not modem.login():
        logger.error("Failed to login into router")
        print("Failed to login into router")
        report_error("Failed to login into router")
        return

    modem_devices = modem.get_devices()
    reboot_modem_if_no_internet_connection(modem)
    modem.logout()

    if modem_devices is None:
        logger.error("No devices retrieved from router")
        print("No devices retrieved from router")
        return

    devices = map_to_devices(modem, modem_devices)

    try:
        update_db(devices)
    except Exception as e:
        logger.error("Failed to update database with new connections")
        logger.exception(e, stack_info=True)
        report_error("Failed to update database with new connections")

    desktop, laptop, phone = setup_devices(modem, modem_devices)

    # Light switch logic
    handle_control_logic(desktop, laptop, phone)

    save_state([phone, laptop, desktop])

    mqtt.disconnect()


def get_modem() -> Optional[Modem]:
    if settings.router == "ConnectboxGiga":
        return ConnectboxGiga(settings.router_ip_address, settings.router_password)

    if settings.router == "ConnectboxCompal":
        try:
            decoded_password = base64.b64decode(settings.router_password).decode("utf-8")
        except binascii.Error:
            logger.info("Password is not (correctly) base64 encoded")
            decoded_password = settings.router_password

        return ConnextboxCompal(settings.router_ip_address, decoded_password)

    if settings.router == "ZTE_MF286C":
        return ZTE_MF286C(settings.router_ip_address, settings.router_password)

    return None


def map_to_devices(modem: Modem, devices: List[ModemDevice]) -> List[Device]:
    return list(map(lambda it: Device(
        name=it.name,
        mac_address=it.mac_address,
        is_connected=it.active,
        ip=it.ipv4_address,
        interface=it.interface,
        room=modem.get_room(it),
        modem_device_active=it.active,
    ), devices))


def setup_devices(modem: Modem, devices: List[ModemDevice]) -> Tuple[Device, Device, Device]:
    # Process devices info
    phone = Device("phone", settings.mac_address_phone)
    phone.retrieve_status(modem, devices)

    laptop = Device("laptop", settings.mac_address_laptop)
    laptop.retrieve_status(modem, devices)

    desktop = Device("desktop", settings.mac_address_desktop)
    desktop.retrieve_status(modem, devices)

    logger.info("Device statuses: ")
    phone.print_status()
    laptop.print_status()
    desktop.print_status()
    return desktop, laptop, phone


def handle_control_logic(desktop, laptop, phone) -> None:
    module = load_control_logic_module()
    module.run(desktop, laptop, phone)


def load_control_logic_module():
    file_name, file_extension = os.path.splitext(settings.control_logic)
    if file_extension != ".py":
        raise ImportError("Wrong file extension for control logics script. Must be .py file.")

    module_path = os.path.join(settings.BASE_DIR, "control_logics", file_name + ".py")
    if not os.path.exists(module_path):
        raise FileNotFoundError("Control logic file not found: {}".format(module_path))

    module_name = "control_logics.{}".format(file_name)
    if __package__ is not None:
        module_name = __package__ + "." + module_name

    return importlib.import_module(module_name, ".")


def update_db(devices: Optional[List[Device]]):
    db.connect()

    with db.session() as session:
        check_for_new_registered_devices(session, devices)

        update_db_connection_states(devices, session)

        session.commit()


def check_for_new_registered_devices(session: Session, devices: Optional[List[Device]]):
    for device in devices:
        db_device = session.query(Connection) \
            .where(Connection.mac_address == device.mac_address).first()

        if db_device is not None:
            # We know this device
            continue

        logger.info("New device registered to router: {}".format(device))
        report_error("New device registered to router: [{}] {} @ {}".format(device.mac_address, device.name, device.ip))


def update_db_connection_states(devices, session):
    old_connected_devices = session.query(Connection) \
        .where(Connection.disconnected_at == None) \
        .order_by(Connection.connected_at.desc()) \
        .all()
    # Check for closed connections
    for old_connected_device in old_connected_devices:
        if devices is None or len(devices) == 0:
            logger.info("Closing connection: " + str(old_connected_device))
            old_connected_device.disconnected_at = datetime.datetime.now()
            continue

        # Check if connection still is open
        current_connected_device = next((it for it in devices
                                         if it.mac_address.lower() == old_connected_device.mac_address.lower()),
                                        None)

        # If not, save the new closed state
        if current_connected_device is None:
            logger.info("Closing connection: " + str(old_connected_device))
            old_connected_device.disconnected_at = datetime.datetime.now()
            continue
    # Check for new connections
    for current_connected_device in devices:
        connection = map_device_to_connection(current_connected_device)

        if old_connected_devices is None or len(old_connected_devices) == 0:
            logger.info("Open new connection: " + str(connection))
            session.add(connection)
            continue

        # Check if connection is already open
        old_connected_device = next((it for it in old_connected_devices
                                     if it.mac_address.lower() == current_connected_device.mac_address.lower()),
                                    None)

        # If not, save the new open state
        if old_connected_device is None:
            logger.info("Open new connection: " + str(connection))
            session.add(connection)
            continue


def map_device_to_connection(device: Device):
    return Connection(
        device_name=device.name,
        mac_address=device.mac_address,
        ip=device.ip,
        interface=device.interface,
        room=device.room,
        connected_at=datetime.datetime.now(),
        disconnected_at=None,
    )


if __name__ == "__main__":
    run()
