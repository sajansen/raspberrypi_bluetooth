import logging

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, declarative_base

from wifi_scanner import settings

logger = logging.getLogger(__name__)

Base = declarative_base()

_engine = None


def connect():
    global _engine
    logger.info("Connecting to database: {}".format(settings.db_uri))
    _engine = create_engine(settings.db_uri, echo=settings.db_query_logging, future=True)

    Base.metadata.create_all(_engine)


def session() -> Session:
    return Session(_engine)
