
# Router

Two routers are supported:
- Connectbox Compal
- Connectbox Giga

Change `settings.router` to one of the following values to change the router to use (connect to):
- `router = "ConnectboxCompal"`
- `router = "ConnectboxGiga"`

Note: admin password for Connectbox Compal can be encrypted with base64

Note: admin password for Connectbox Giga must be obtained from the network request on the login page. This because I didn't want to implement al those client side encryptions. Just log in into the router admin page and copy the data send to the password check endpoint.

# Control logic

Add a custom control logic file wo control_logics/. Select this file by changing `settings.control_logic` to this file name (including extension which must be '.py'). Example: 
```python
# settings.py
control_logic = "logic1.py" # Use logic in control_logics/logic1.py
```

This file must contain at least the following:
```python
def run(desktop: Device, laptop: Device, phone: Device) -> None:
    # Insert your logic here
    pass
```

The `run(desktop, laptop, device)` function will run every time with the updated values from the router. 