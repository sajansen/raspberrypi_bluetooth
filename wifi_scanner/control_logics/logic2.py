"""Control logic which turns on/off the lights when a device enters/leaves the house,
or when the desktop PC is turned on. If a device connects to the first floor router, coming from downstairs,
lights will also be turned on."""
import logging

from wifi_scanner.app import lights_off, lights_on
from wifi_scanner.objects import Device, Room
from wifi_scanner.state import connection_changed, \
    room_changed, previous_room

logger = logging.getLogger(__name__)


def run(desktop: Device, laptop: Device, phone: Device) -> None:
    print()

    # If all disconnected or away
    if not desktop.is_connected \
            and not phone.is_connected \
            and not phone.is_connected:
        logger.info("Not a single device connected")
        return lights_off()

    if connection_changed(desktop):
        # If desktop turns on
        if desktop.is_connected:
            logger.info("Desktop just turned on")
            return lights_on()

    if connection_changed(phone):
        # If phone turns on / enters the house
        if phone.is_connected:
            logger.info("Phone just entered the house")
            return lights_on()

        # If phone leaves the house
        elif not phone.is_connected:
            logger.info("Phone just left the house")
            lights_off()

    if connection_changed(laptop):
        # If laptop turns on / enters the house
        if laptop.is_connected:
            logger.info("Laptop just turned on")
            return lights_on()

        # If laptop turns off and phone is disconnected
        elif not laptop.is_connected and not phone.is_connected:
            logger.info("Laptop just turned off (and phone is not connected)")
            lights_off()

    if room_changed(phone):
        # If phone enters the room
        if previous_room(phone) == Room.Downstairs and phone.room == Room.FirstFloor:
            logger.info("Phone just entered the FirstFloor")
            lights_on()

    if room_changed(laptop):
        # If laptop enters the room
        if previous_room(laptop) == Room.Downstairs and laptop.room == Room.FirstFloor:
            logger.info("Laptop just entered the FirstFloor")
            lights_on()
