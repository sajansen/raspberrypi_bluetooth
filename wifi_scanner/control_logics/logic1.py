"""Control logic which turns on/off the lights when a device enters/leaves the room, or when the desktop PC is turned on"""
import logging

from wifi_scanner.app import lights_off, lights_on
from wifi_scanner.objects import Device, Room
from wifi_scanner.state import connection_changed, \
    room_changed

logger = logging.getLogger(__name__)


def run(desktop: Device, laptop: Device, phone: Device) -> None:
    print()

    # If all disconnected or away
    if not desktop.is_connected \
            and phone.room != Room.FirstFloor \
            and laptop.room != Room.FirstFloor:
        logger.info("Not a single device in the room")
        return lights_off()

    if connection_changed(desktop):
        # If desktop turns on
        if desktop.is_connected:
            logger.info("Desktop just turned on")
            lights_on()

    if room_changed(phone) or room_changed(laptop):
        # If phone and laptop have left the room
        if phone.room != Room.FirstFloor and laptop.room != Room.FirstFloor:
            logger.info("Phone and/or laptop have left the FirstFloor")
            lights_off()

        # If phone enters the room
        elif phone.room == Room.FirstFloor and room_changed(phone):
            logger.info("Phone just entered the FirstFloor")
            lights_on()

        # If laptop enters the room
        elif laptop.room == Room.FirstFloor and room_changed(laptop):
            logger.info("Phone just entered the FirstFloor")
            lights_on()
