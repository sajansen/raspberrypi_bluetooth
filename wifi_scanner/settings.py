import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Control logic file name from control_logics/ directory
control_logic = "logic1.py"

# Database
db_uri = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
db_query_logging = False

# Choose between ConnectboxGiga and ConnectboxCompal
router = "ZTE_MF286C"
router_ip_address = "192.168.0.1"
router_password = ''
mac_address_phone = ""
mac_address_laptop = ""
mac_address_desktop = ""

enable_lights_on = True
enable_lights_off = True

state_file = os.path.join(BASE_DIR, "wifi_scanner_state.json")

# MQTT
MQTT_TOPIC = "light_controller"
MQTT_HOST = "localhost"
MQTT_PORT = 1883
MQTT_USERNAME = "admin"
MQTT_PASSWORD = "admin"

localsettingspath = os.path.join(BASE_DIR, "settings_local.py")
if os.path.exists(localsettingspath):
    from wifi_scanner.settings_local import *
