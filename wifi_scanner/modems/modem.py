from typing import Optional, List

from wifi_scanner.objects import ModemDevice, Room


class Modem:
    def __init__(self, ip: str, password: str):
        self.ip = ip
        self.password = password

    def login(self) -> bool:
        pass

    def logout(self):
        pass

    def get_devices(self) -> Optional[List[ModemDevice]]:
        pass

    def get_room(self, device: ModemDevice) -> Room:
        pass

    def check_internet_connection(self) -> bool:
        return True

    def reboot(self):
        pass
