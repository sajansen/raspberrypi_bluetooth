import hashlib
import logging
import sys
from datetime import datetime
from typing import Optional, List

import requests

from wifi_scanner import settings
from wifi_scanner.modems.modem import Modem
from wifi_scanner.objects import ModemDevice, Room

logger = logging.getLogger(__name__)


class ZTE_MF286C(Modem):
    def __init__(self, ip: str, password: str):
        super().__init__(ip, password)

        self.logged_in = False
        self.cookie = None

    def login(self) -> bool:
        logger.info("Logging in...")
        self.logged_in = False

        data = "isTest=false&goformId=LOGIN&password={}".format(self.password)
        headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Referer": "http://{}/index.html".format(self.ip),
        }
        request = requests.post("http://{}/goform/goform_set_cmd_process".format(self.ip),
                                headers=headers, data=data)

        if request.status_code != 200:
            logger.error("Failed to log in into router: wrong status response. Response: (%s) %s",
                         request.status_code, request.text)
            return False

        result = request.json()

        if result['result'] != '0':
            logger.error("Failed to log in into router: wrong password")
            return False

        self.cookie = request.cookies.get('zwsd')
        self.logged_in = True
        logger.info("Logged in with cookie zwsd: %s", self.cookie)

        return True

    def logout(self):
        self._post("/goform/goform_set_cmd_process", data="isTest=false&goformId=LOGOUT")
        self.logged_in = False
        logger.info("Logged out")

    def get_devices(self) -> Optional[List[ModemDevice]]:
        logger.info("Getting devices...")

        if not self.logged_in:
            logger.error("Could not get devices: not logged in")
            return None

        request = self._get("/goform/goform_get_cmd_process?isTest=false&cmd=station_list&_={}"
                            .format(self._timestamp()))

        if request.status_code != 200:
            logger.error("Failed to get devices. Response: (%s) %s", request.status_code, request.text)
            return None

        result = request.json()

        return self._result_to_device_list(result)

    def _result_to_device_list(self, result) -> Optional[List[ModemDevice]]:
        output = list()
        for device in result['station_list']:
            name = device['hostname']
            mac_address = device['mac_addr']
            ipv4_address = device['ip_addr']
            interface = 'WiFi'
            active = True

            output.append(ModemDevice(
                name=name,
                mac_address=mac_address,
                ipv4_address=ipv4_address,
                interface=interface,
                active=active,
            ))
        return output

    def _get(self, endpoint: str):
        if not self.logged_in:
            logger.error("Could not perform GET request: not logged in")
            return

        headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Referer": "http://{}/index.html".format(self.ip),
            "Cookie": "zwsd={}".format(self.cookie),
        }

        request_url = "http://{}{}".format(self.ip, endpoint)
        request = requests.get(request_url, headers=headers)

        logger.debug("({}) Request status_code: {}".format(request_url, request.status_code))
        logger.debug("({}) Request text: {}".format(request_url, request.text))

        return request

    def _post(self, endpoint: str, data: str):
        if not self.logged_in:
            logger.error("Could not perform POST request: not logged in")
            return

        headers = {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Referer": "http://{}/index.html".format(self.ip),
            "Origin": "http://{}".format(self.ip),
            "Cookie": "zwsd={}".format(self.cookie),
        }

        request_url = "http://{}{}".format(self.ip, endpoint)
        request = requests.post(request_url, headers=headers, data=data)

        logger.debug("({}) Request status_code: {}".format(request_url, request.status_code))
        logger.debug("({}) Request text: {}".format(request_url, request.text))

        return request

    def _timestamp(self):
        return round(datetime.now().timestamp() * 1000)

    def get_room(self, device: ModemDevice) -> Room:
        return Room.FirstFloor

    def check_internet_connection(self) -> bool:
        logger.info("Checking internet connection...")

        if not self.logged_in:
            logger.error("Could not get status: not logged in")
            return True

        request = self._get(
            "/goform/goform_get_cmd_process?multi_data=1&isTest=false&sms_received_flag_flag=0&sts_received_flag_flag=0&cmd=ppp_status&_={}"
                .format(self._timestamp()))

        if request.status_code != 200:
            logger.error("Failed to get internet connection status. Response: (%s) %s",
                         request.status_code, request.text)
            return True

        result = request.json()

        return result["ppp_status"] == "ppp_connected"

    def reboot(self):
        logger.info("Rebooting modem...")

        if not self.logged_in:
            logger.error("Could not reboot: not logged in")
            return None

        # Get device versions
        request = self._get(
            "/goform/goform_get_cmd_process?isTest=false&cmd=cr_version%2Cwa_inner_version&multi_data=1&_={}"
                .format(self._timestamp()))

        if request.status_code != 200:
            logger.error("Failed to get device versions. Response: (%s) %s",
                         request.status_code, request.text)

        result = request.json()
        cr_version = result["cr_version"]
        wa_inner_version = result["wa_inner_version"]

        request = self._get("/goform/goform_get_cmd_process?isTest=false&cmd=RD&_={}"
                            .format(self._timestamp()))

        # Get RD value
        if request.status_code != 200:
            logger.error("Failed to get RD. Response: (%s) %s",
                         request.status_code, request.text)

        result = request.json()
        RD = result["RD"]

        # Create secret hash
        AD = hashlib.md5((hashlib.md5((wa_inner_version + cr_version).encode()).hexdigest() + RD).encode()).hexdigest()

        # Submit reboot request
        request = self._post("/goform/goform_set_cmd_process",
                             "isTest=false&goformId=REBOOT_DEVICE&AD={}".format(AD))

        if request.status_code != 200:
            logger.error("Failed to get submit reboot request. Response: (%s) %s",
                         request.status_code, request.text)

        result = request.json()
        if result["result"] != "success":
            logger.error("Failed to reboot request. Response: %s", result['result'])


def run():
    password = settings.router_password

    m = ZTE_MF286C(settings.router_ip_address, password)
    if not m.login():
        return

    print("Getting devices")
    print(m.reboot())
    print("Done")

    m.logout()


if __name__ == "__main__":
    logging.basicConfig(
        stream=sys.stdout,
        format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
        level=logging.DEBUG)
    run()
