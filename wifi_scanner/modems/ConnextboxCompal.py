import logging
from typing import Optional, List

from compal import *

from wifi_scanner.modems.modem import Modem
from wifi_scanner.objects import ModemDevice, Room

logger = logging.getLogger(__name__)


class ConnextboxCompal(Modem):
    def __init__(self, ip: str, password: str):
        super().__init__(ip, password)

        self.modem = Compal(ip, password)

    def login(self) -> bool:
        logger.info("Connecting to router at {}".format(self.ip))
        try:
            self.modem.login()
        except Exception as e:
            logger.error("Failed to log into router")
            logger.exception(e, exc_info=True)
            return False
        return True

    def logout(self):
        self.modem.logout()

    def get_devices(self) -> Optional[List[ModemDevice]]:
        table = LanTable(self.modem)

        output = list()
        for devices in table.table.values():
            if not isinstance(devices, list):
                continue

            for device in devices:
                output.append(ModemDevice(
                        name=device.get('hostname'),
                        mac_address=device.get('MACAddr').upper(),
                        ipv4_address=device.get('IPv4Addr'),
                        interface=device.get('interface'),
                ))
        return output

    def get_room(self, device: ModemDevice) -> Room:
        if 'Ethernet' in device.interface:
            return Room.FirstFloor
        return Room.Downstairs
