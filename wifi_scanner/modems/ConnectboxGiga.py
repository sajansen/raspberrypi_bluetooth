import logging
from typing import Optional, List

import requests

from wifi_scanner import settings
from wifi_scanner.modems.modem import Modem
from wifi_scanner.objects import ModemDevice, Room

logger = logging.getLogger(__name__)


class ConnectboxGiga(Modem):
    def __init__(self, ip: str, password: str):
        super().__init__(ip, password)

        self.logged_in = False
        self.csrf_nonce = None
        self.session_id = None
        self.credential = "eyAidW5pcXVlIjoiMjgwb2FQU0xpRiIsICJmYW1pbHkiOiI4NTIiLCAibW9kZWxuYW1lIjoiVEcyNDkyTEctODUiLCAibmFtZSI6InRlY2huaWNpYW4iLCAidGVjaCI6dHJ1ZSwgIm1vY2EiOjAsICJ3aWZpIjo1LCAiY29uVHlwZSI6IkxBTiIsICJnd1dhbiI6ImYiLCAiRGVmUGFzc3dkQ2hhbmdlZCI6IllFUyIgfQ=="

    def login(self) -> bool:
        logger.info("Logging in...")
        self.logged_in = False

        data = self.password
        headers = {"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"}
        request = requests.post("http://{}/php/ajaxSet_Password.php".format(self.ip),
                                headers=headers, data=data)

        if request.status_code != 200:
            logger.error("Failed to log in into router: wrong status response. Response: (%s) %s",
                         request.status_code, request.text)
            return False

        result = request.json()

        if result['p_status'] == 'Active_session':
            logger.error("Failed to log in into router: router already has an active session")
            return False
        elif result['p_status'] != 'Match':
            logger.error("Failed to log in into router: wrong password")
            return False

        self.csrf_nonce = result['nonce']
        self.session_id = request.cookies.get('PHPSESSID')
        self.logged_in = True
        logger.info("Logged in with CSRF: %s, PHPSESSID: %s", self.csrf_nonce, self.session_id)

        return True

    def logout(self):
        self._post("/php/logout.php", "")

    def get_devices(self) -> Optional[List[ModemDevice]]:
        logger.info("Getting devices...")

        if not self.logged_in:
            logger.error("Could not get devices: not logged in")
            return None

        request = self._post("/php/associated_devices_data.php",
                             "assocDevData=%7B%22numHostEnt%22%3A%22%22%2C%22hostName%22%3A%22%22%2C%22phyAddress%22%3A%22%22%2C%22ipAddress%22%3A%22%22%2C%22layer1Interface%22%3A%22%22%2C%22maxBitRate%22%3A%22%22%2C%22lastDownLinkRate%22%3A%22%22%2C%22active%22%3A%22%22%2C%22FriendlyName%22%3A%22%22%2C%22SSID%22%3A%22%22%2C%22HotSpot%22%3A%22%22%2C%22ipv6Address%22%3A%22%22%7D&opType=READ")

        if request.status_code != 200:
            logger.error("Failed to get devices. Response: (%s) %s", request.status_code, request.text)
            return None

        result = request.json()

        return self._result_to_device_list(result)

    def _result_to_device_list(self, result) -> Optional[List[ModemDevice]]:
        output = list()
        for i in range(int(result['numHostEnt'])):
            name = result['hostName'][i]
            mac_address = result['phyAddress'][i].upper()
            ipv4_address = result['ipAddress'][i]
            interface = result['layer1Interface'][i]
            active = result['active'][i] == 'true'

            output.append(ModemDevice(
                    name=name,
                    mac_address=mac_address,
                    ipv4_address=ipv4_address,
                    interface=interface,
                    active=active,
            ))
        return output

    def _post(self, endpoint: str, data: str):
        if not self.logged_in:
            logger.error("Could not perform POST request: not logged in")
            return

        headers = {
            "Accept": "application/json, text/javascript, */*; q=0.01",
            "Accept-Language": "en-US,en;q=0.5",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "CSRF_NONCE": self.csrf_nonce,
            "Origin": "http://{}".format(self.ip),
            "Cookie": "PHPSESSID={}".format(self.session_id),
        }

        request_url = "http://{}{}".format(self.ip, endpoint)
        request = requests.post(request_url,
                                headers=headers, data=data)

        logger.debug("({}) Request status_code: {}".format(request_url, request.status_code))
        logger.debug("({}) Request text: {}".format(request_url, request.text))

        return request

    def get_room(self, device: ModemDevice) -> Room:
        if 'Ethernet' in device.interface:
            return Room.FirstFloor
        return Room.Downstairs


def run():
    password = "configInfo=%7B%22encryptedBlob%22%3A%2276b401f59b0bbee020614e3c72ee621d617490e1c9b811872c2c170176391d27f649cbff5910b18eba6ad6c0c7d609d9f57ddd611bf6700b94e3e09befd821b6540f77bacf283f370dc4db61fbeaab65c9c8c5725820f26724b48ceea4d4bd299575733fccefde9bb3bf58bb70498239465f3e2fd7c12461554e75f086a890b3b7814ca3d3ee55a4ad124eb39c5934c6b2e29c897c5f1c03b86ac9d19df559d530974795954dcefa12d803ca22dff822354293bea40f15d0a75c1ce9740fed059f1110%22%2C%22salt%22%3A%223410a8f2695e3d4f%22%2C%22iv%22%3A%22266bb6581e99d289%22%2C%22authData%22%3A%22encryptData%22%7D"

    m = ConnectboxGiga(settings.router_ip_address, password)
    if not m.login():
        return

    print("Getting devices")
    m.get_devices()
    print("Done")

    m.logout()


if __name__ == "__main__":
    logging.basicConfig(
            format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
            level=logging.DEBUG)
    run()
