from sqlalchemy import Column, Integer, DateTime, String, Enum

from wifi_scanner.db import Base
from wifi_scanner.objects import Room


class Connection(Base):
    __tablename__ = 'connections'

    id = Column(Integer, primary_key=True)
    device_name = Column(String)
    mac_address = Column(String)
    ip = Column(String)
    interface = Column(String)
    room = Column(Enum(Room))
    connected_at = Column(DateTime, nullable=False)
    disconnected_at = Column(DateTime, nullable=True)

    def __repr__(self):
        return "Connection(id={}, device_name={}, mac_address={}, " \
               "ip={}, interface={}, room={}, connected_at={}, disconnected_at={})" \
            .format(self.id,
                    self.device_name,
                    self.mac_address,
                    self.ip,
                    self.interface,
                    self.room.name,
                    self.connected_at,
                    self.disconnected_at, )
