import json
import logging
import os
from datetime import datetime
from typing import List, Optional

from wifi_scanner import settings
from wifi_scanner.objects import MyJsonEncoder, Device, Room, State

logger = logging.getLogger(__name__)

previous_state: Optional[State] = None


def save_state(devices: List[Device]) -> None:
    data_object = State(
        timestamp=datetime.now(),
        devices=devices
    )
    data = json.dumps(data_object, cls=MyJsonEncoder)

    logger.info("Saving state to file: {}".format(settings.state_file))
    with open(settings.state_file, 'w') as file:
        file.write(data)

    logger.info("Done saving state")


def get_state() -> State:
    logger.info("Getting state from file: {}".format(settings.state_file))

    if not os.path.isfile(settings.state_file):
        return State(None, [])

    with open(settings.state_file, 'r') as file:
        file_content = file.read()

    data_object = json.loads(file_content)

    devices = []
    for data_device in data_object["devices"]:
        device = Device(data_device["name"], data_device["mac_address"])
        device.is_connected = data_device["is_connected"]
        device.ip = data_device["ip"]
        device.interface = data_device["interface"]
        device.room = Room[data_device["room"]] if data_device["room"] is not None else None
        devices.append(device)

    return State(
        timestamp=datetime.strptime(data_object["timestamp"].split(".")[0], "%Y-%m-%dT%H:%M:%S"),
        devices=devices
    )


def get_previous_state_for(device: Device) -> Optional[Device]:
    global previous_state
    if previous_state is None:
        previous_state = get_state()

    try:
        return next(d for d in previous_state.devices if d.name == device.name)
    except StopIteration:
        logger.info("Device '{}' not found in previous state".format(device.name))
        return None


def state_changed(now: Device, previous: Optional[Device] = None) -> bool:
    if previous is None:
        previous = get_previous_state_for(now)

        if previous is None:
            return True

    return connection_changed(now, previous) or room_changed(now, previous)


def connection_changed(now: Device, previous: Optional[Device] = None) -> bool:
    if previous is None:
        previous = get_previous_state_for(now)

        if previous is None:
            return True

    return now.is_connected != previous.is_connected


def room_changed(now: Device, previous: Optional[Device] = None) -> bool:
    if previous is None:
        previous = get_previous_state_for(now)

        if previous is None:
            return True

    return now.room != previous.room


def previous_room(now: Device, previous: Optional[Device] = None) -> Room:
    if previous is None:
        previous = get_previous_state_for(now)

        if previous is None:
            return Room.Unknown

    return previous.room
