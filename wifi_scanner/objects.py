import enum
import logging
from datetime import datetime
from json import JSONEncoder
from typing import List, Optional

logger = logging.getLogger(__name__)


class MyJsonEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Room):
            return o.name
        if isinstance(o, datetime):
            return o.isoformat()
        if isinstance(o, Device):
            return o.__dict__
        if isinstance(o, State):
            return o.__dict__
        return super(MyJsonEncoder, self).default(o)


class Room(enum.Enum):
    Unknown = -1
    Downstairs = 0
    FirstFloor = 1


class ModemDevice:
    def __init__(self,
                 name: str,
                 mac_address: str,
                 ipv4_address: str,
                 interface: str,
                 active: bool = True):
        self.name: str = name
        self.mac_address: str = mac_address
        self.ipv4_address: str = ipv4_address
        self.interface: str = interface
        self.active: bool = active

    def __repr__(self):
        return "ModemDevice[name={}, mac_addres={}, ipv4_address={}, interface={}, active={}]".format(
            self.name, self.mac_address, self.ipv4_address, self.interface, self.active
        )


class Device:
    def __init__(self, name: str,
                 mac_address: str,
                 is_connected: bool = False,
                 ip: str = None,
                 interface: str = None,
                 room: Optional[Room] = None,
                 modem_device_active: bool = False,
                 ):
        self.name = name
        self.mac_address = mac_address.upper()
        self.is_connected = is_connected
        self.ip = ip
        self.interface = interface
        self.room = room
        self.modem_device_active = modem_device_active

    def __repr__(self):
        return "Device[name={}, mac_addres={}, ip={}, interface={}, is_connected={}, room={}, modem_device_active={}]".format(
            self.name, self.mac_address, self.ip, self.interface, self.is_connected, self.room, self.modem_device_active
        )

    def json_serialize(self) -> dict:
        return {
            "name": self.name,
            "mac_address": self.mac_address,
            "is_connected": self.is_connected,
            "ip": self.ip,
            "interface": self.interface,
            "room": self.room,
        }

    def retrieve_status(self, modem, devices: List[ModemDevice]) -> None:
        try:
            modem_device = next(device for device in devices if device.mac_address == self.mac_address)
        except StopIteration:
            logger.info("{} not found in modem devices".format(self.name))
            modem_device = None

        if modem_device is None:
            self.is_connected = False
            self.ip = None
            self.interface = None
            self.modem_device_active = False
            self.room = None
            return

        self.is_connected = modem_device.active
        self.ip = modem_device.ipv4_address
        self.interface = modem_device.interface
        self.modem_device_active = modem_device.active
        self.room = modem.get_room(modem_device)

    def print_status(self) -> None:
        if not self.is_connected:
            logger.info("{} is not connected".format(self.name))
            logger.info("( - Active:     {})".format(self.modem_device_active))
            return

        logger.info("{} is connected".format(self.name))
        logger.info("  - IP:         {}".format(self.ip))
        logger.info("  - Interface:  {}".format(self.interface))
        logger.info("  - Room:       {}".format(self.room.name))
        logger.info("( - Active:     {})".format(self.modem_device_active))


class State:
    def __init__(self, timestamp: Optional[datetime], devices: List[Device]):
        self.timestamp: Optional[datetime] = timestamp
        self.devices: List[Device] = devices
