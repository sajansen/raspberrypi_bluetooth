import logging
import time

import paho.mqtt.client as mqtt

logger = logging.getLogger(__name__)


class MQTT:
    def __init__(self,
                 host: str = "localhost",
                 port: str = 1883,
                 username: str = "guest",
                 password: str = "guest", ):
        self._client: mqtt.Client = mqtt.Client()
        self._is_connected = False

        self.host = "localhost"
        self.port = 1883
        self.username = "guest"
        self.password = "guest"

    def client(self):
        return self._client

    def connect(self, max_delay: int = 0):
        self._client.on_connect = self.on_connect

        self._client.username_pw_set(self.username, self.password)

        try:
            self._client.connect(self.host, self.port, 60)
        except Exception as e:
            logger.error("Failed to connect to MQTT")
            raise e

        self._client.loop_start()
        logger.info("MQTT connection created on {}".format(self.host))

        if max_delay <= 0:
            return

        remaining_delay = max_delay
        while remaining_delay > 0 and not self._is_connected:
            logger.info("  waiting for MQTT connection ({} sec)...".format(remaining_delay))
            time.sleep(1)
            remaining_delay = remaining_delay - 1

    def try_connect(self, max_delay: int = 0, log_exception: bool = True):
        try:
            self.connect(max_delay)
        except Exception as e:
            logger.error("Failed to connect to MQTT broker")
            if log_exception:
                logger.exception(e, stack_info=True)

    def is_connected(self):
        return self._is_connected

    def disconnect(self, ):
        logger.info("Stopping MQTT connection")
        try:
            self._client.loop_stop()
            self._client.disconnect()
        except Exception as e:
            logger.warning("Failure when disconnecting to MQTT broker")
            logger.exception(e, stack_info=True)

    # The callback for when the self.mqtt_client receives a CONNACK response from the server.
    def on_connect(self, client: mqtt.Client, userdata, flags, rc: int):
        if rc == mqtt.CONNACK_ACCEPTED:
            logger.info("Connected to MQTT broker")
            self._is_connected = True
        else:
            logger.warning("Connection result code ({}): {}".format(rc, mqtt.connack_string(rc)))
            raise ConnectionError("Failed to connect to MQTT broker: {}".format(mqtt.connack_string(rc)))

    def send(self, topic: str, message: str):
        logger.debug("Sending message to topic: {} -> {}".format(message, topic))
        self._client.publish(topic, message)

    def try_send(self, topic: str, message: str):
        if not self.is_connected():
            return

        try:
            self.send(topic, message)
        except Exception as e:
            logger.error("Failed to send message to MQTT broker")
            logger.exception(e, stack_info=True)
