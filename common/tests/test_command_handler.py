import unittest

from common.command_handler import Command


class CommandHandlerTest(unittest.TestCase):

  def test_command_match_function_with_normal_regex(self):
    command = Command(r"^light (brightness|level|set) (?P<value>\d+) ?(?P<duration>[\d\.]+)? ?(?P<timeout>[\d\.]+)?$",
                      None)

    self.assertTrue(command.match("light level 10 10.0 0.2"))
    self.assertTrue(command.match("light level 10 20 0"))
    self.assertTrue(command.match("light level 10 1"))
    self.assertTrue(command.match("light level 10"))
    self.assertFalse(command.match("light level"))
    self.assertFalse(command.match("else level 10 10.0 0.2"))
    self.assertFalse(command.match("light level 10 abc 0.2"))

  def test_command_match_function_with_advanced_regex(self):
    command = Command(r"^light <str:type> <int:value> ?<float:duration>? ?<float:timeout>?$", None)

    self.assertTrue(command.match("light level 10 10.0 0.2"))
    self.assertTrue(command.match("light level 10 20 0"))
    self.assertTrue(command.match("light level 10 1"))
    self.assertTrue(command.match("light level 10"))
    self.assertFalse(command.match("light level"))
    self.assertFalse(command.match("else level 10 10.0 0.2"))
    self.assertFalse(command.match("light level 10 abc 0.2"))

  def test_command_execute_function_gives_correct_parameters_and_removes_unset_parameters(self):
    def callback(**params):
      self.assertEqual(["type", "value", "duration"], list(params.keys()))
      self.assertEqual(["level", "10", "10.0"], list(params.values()))

    command = Command(r"^light <str:type> (?P<value>\d+) ?<float:duration>? ?<float:timeout>?$", callback)

    command.execute("light level 10 10.0", None)

  def test_command_execute_function_with_any(self):
    def callback(**params):
      self.assertEqual(["type", "line"], list(params.keys()))
      self.assertEqual(["level", "10 words are needed"], list(params.values()))

    command = Command(r"^light <str:type> <any:line>$", callback)

    command.execute("light level 10 words are needed", None)
