import time


def sleep_ms(milliseconds: float):
    target_time = time.time() + milliseconds / 1000.0
    while time.time() < target_time:
        pass
