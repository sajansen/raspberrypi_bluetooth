import logging
import re
from typing import List, Optional

logger = logging.getLogger(__name__)


class Command:
  def __init__(self, matcher: str, callback):
    self.matcher = self.enrich_matcher(matcher)
    self.callback = callback

  def enrich_matcher(self, matcher: str):
    matcher = re.sub(r"<float:(.*?)>", r"(?P<\1>[\\d\.]+)", matcher)
    matcher = re.sub(r"<int:(.*?)>", r"(?P<\1>\\d+)", matcher)
    matcher = re.sub(r"<str:(.*?)>", r"(?P<\1>\\w+)", matcher)
    matcher = re.sub(r"<any:(.*?)>", r"(?P<\1>.+)", matcher)
    return matcher

  def match(self, text: str) -> bool:
    if not re.match(self.matcher, text):
      return False
    return True

  def execute(self, command_string: str, caller: any = None):
    result = re.search(self.matcher, command_string)
    arguments = result.groupdict()

    # Remove values not set
    for key, value in result.groupdict().items():
      if value is None:
        del arguments[key]

    if caller is not None:
      self.callback(caller, **arguments)
    else:
      self.callback(**arguments)


def find_matching_command(commands: List[Command], text: str) -> Optional[Command]:
  for command in commands:
    if command.match(text):
      return command

  return None
