#!/bin/bash

SCRIPTPATH="$(
  cd "$(dirname "$0")"
  pwd -P
)"
CRONJOB="35 6 * * * ${SCRIPTPATH}/wake_me_up.sh | /usr/bin/logger -t wake_me_up_service"

echo "Adding to crontab"
crontab -l |
  grep -v -F "wake_me_up_service" |   # Remove any possible existing line for this service and
  {
    cat
    echo "${CRONJOB}"                 # append this service CRONJOb to the end of the crontab
  } |
  crontab -

echo "Done :)"
