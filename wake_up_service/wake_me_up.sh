#!/bin/bash

DURATION_IN_MINUTES=15
DEFAULT_TOPIC="light_controller"
WAKEUP_MUSIC_FILE="/home/pi/Music/wake-up-birds.mp3"

DURATION_IN_SECONDS=$((DURATION_IN_MINUTES * 60))
echo "Waking up during the next ${DURATION_IN_MINUTES} minutes... "

# Get topic name
SCRIPTPATH="$(
  cd "$(dirname "$0")"
  pwd -P
)"
. "${SCRIPTPATH}/../venv/bin/activate"
TOPIC="$(
  cd "${SCRIPTPATH}/../light_controller"
  python -c "import settings; print(settings.MQTT_TOPIC)" || echo "${DEFAULT_TOPIC}"
)"
DESK_MAX_BRIGHTNESS="$(
  cd "${SCRIPTPATH}/../light_controller"
  python -c "import settings; print(settings.MAX_BRIGHTNESS)" || echo "1024"
)"

function sendCommand() {
    rabbitmqadmin publish exchange=amq.topic routing_key="${TOPIC}" payload="$1"
}

# Prepare wake up
echo "Preparing for wake up..."

sendCommand "movinghead effect off"
sendCommand "movinghead color 0 0 0"
sendCommand "movinghead brightness 255"

sendCommand "movinghead effect solid"
sendCommand "movinghead color 255 60 10 ${DURATION_IN_SECONDS}"
sendCommand "desk brightness ${DESK_MAX_BRIGHTNESS} ${DURATION_IN_SECONDS}"

echo "Waking up sequence initiated"

sleep ${DURATION_IN_SECONDS}
sleep 5
# Make sure PWM is at its max
sendCommand "desk brightness ${DESK_MAX_BRIGHTNESS} 0"
#omxplayer --no-keys "${WAKEUP_MUSIC_FILE}" &