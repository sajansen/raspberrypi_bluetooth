This service receives and decodes messages from MQTT and sends these to an IFTTT webhook. It is used for sending error
messages from services.

## Setup

In order to make this service work, it needs to be run in the background. It will fire up a listen server, which will
monitor MQTT for any new messages.

## Usage

The caller service sends the following JSON message on the MQTT topic ('errors' by default):

```json
{
  "service": "<caller service name>",
  "message": "<error message>"
}
```

## Backend

This service will parse this message and send its data to the configured IFTTT webhook in the following format:

```json
{
  "value1": "<caller service name>",
  "value2": "<error message>"
}
```

Note that if service name was not provided (or fails to parse), it will default to "Unknown service". Also note that if
the error message was not provided (or fails to parse), it will default to an error message telling that this parsing
didn't work and why. 