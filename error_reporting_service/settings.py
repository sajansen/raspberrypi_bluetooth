import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Server
SERVICE_NAME = "pi_error_reporting_service"  # Name of this server running as service

# MQTT
MQTT_TOPIC = "errors"
MQTT_HOST = "localhost"
MQTT_PORT = 1883
MQTT_USERNAME = "admin"
MQTT_PASSWORD = "admin"

# IFTTT
# These values can be found at: https://ifttt.com/maker_webhooks/settings -> visit URL displayed
ifttt_webhook_url_template = "https://maker.ifttt.com/trigger/{event}/with/key/{key}"
ifttt_webhook_key = ""
# This value is set by your applet
ifttt_webhook_event = ""

localsettingspath = os.path.join(BASE_DIR, "settings_local.py")
if os.path.exists(localsettingspath):
    from error_reporting_service.settings_local import *
