import logging
import sys

from error_reporting_service import server, settings

logger = logging.getLogger(__name__)
logging.basicConfig(
    stream=sys.stdout,
    format='[%(levelname)s] %(asctime)s %(name)s | %(message)s',
    level=logging.DEBUG)


def run():
    if settings.ifttt_webhook_key is None or len(settings.ifttt_webhook_key) == 0:
        return logger.error("The IFTTT webhook key must be defined before running this application")
    if settings.ifttt_webhook_event is None or len(settings.ifttt_webhook_event) == 0:
        return logger.error("The IFTTT webhook event name must be defined before running this application")

    client = server.start()
    try:
        client.loop_forever()
    except KeyboardInterrupt:
        pass

    server.stop(client)
    logger.info("Error reporting service stopped.")


if __name__ == '__main__':
    run()
