#!/bin/bash

# Prepare service file
echo "Creating service"
SCRIPTPATH="$(
  cd "$(dirname "$0")"
  pwd -P
)"
PARENTPATH="$(dirname ${SCRIPTPATH})"
cat "${SCRIPTPATH}/pi_error_reporting_service.service.template" | sed "s#\/directorypath#${PARENTPATH}#g" > pi_error_reporting_service.service

# Deploy service file
echo "Deploying service"
sudo cp pi_error_reporting_service.service /etc/systemd/system/
sudo chmod 664 /etc/systemd/system/pi_error_reporting_service.service

# Clean up our garbage
echo "Cleaning up"
rm pi_error_reporting_service.service

# Run service file
echo "Enabling service at boot"
sudo systemctl enable pi_error_reporting_service # Run at boot

echo "Done :)"
