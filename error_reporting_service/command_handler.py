import json
import logging

import requests

from error_reporting_service import settings

logger = logging.getLogger(__name__)


def handle_command(text: str) -> None:
    if text is None or len(text.strip()) == 0:
        logger.info("Ignoring empty message")
        return

    message = json.loads(text)
    try:
        error_service_name = message["service"]
    except Exception as e:
        logger.error("Failed to determine key 'service' from json: {}".format(text))
        logger.exception(e, stack_info=True)
        error_service_name = "Unknown service"

    try:
        error_message = message["message"]
    except Exception as e:
        logger.error("Failed to determine key 'message' from json: {}".format(text))
        logger.exception(e, stack_info=True)
        error_message = "Failed to determine message from JSON. Error: '{}'. JSON: '{}'".format(e, text)

    webhook_data = {
        "value1": str(error_service_name),
        "value2": str(error_message),
        "value3": ""
    }
    send(webhook_data)


def send(data: any):
    headers = {
        "Content-Type": "application/json; charset=UTF-8",
    }

    request_url = settings.ifttt_webhook_url_template\
        .replace("{key}", settings.ifttt_webhook_key)\
        .replace("{event}", settings.ifttt_webhook_event)

    request_data = json.dumps(data)

    logger.info("Sending data to IFTTT webhook: {}".format(request_url))
    request = requests.post(request_url, headers=headers, data=request_data)

    logger.debug("({}) Request status_code: {}".format(request_url, request.status_code))
    logger.debug("({}) Request text: {}".format(request_url, request.text))

    if request.status_code != 200:
        logger.error("Failed to send data to IFTTT webhook")
