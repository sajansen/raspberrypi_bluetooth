# Raspberry PI light controller

_Some services to control lights with PI and bluetooth/time_

## Setup

Set up each module individually:

1. _light_controller_: The main service to control the lights
1. _wake_up_service_: Fade in the lights every morning
1. _bluetooth_server_: Control the lights with a bluetooth connection
1. _backup_service_: Automates backup scripts during nighttime


## Run

All Python services must be called through `entrypoint.py`. This file will make sure everything the service needs, will be there. For example, to run the light controller service, call:

```bash
$ python3 entrypoint.py light_controller
``` 

Call `entrypoint.py help` for the available services.


## Develop

### Add service

Each service goes into it's own folder. The folder name must represent the service name. This name must also be added to the `available_service_names` in `entrypoint.py`. Also, a `app.py` must be available in the folder, with a function `run()`. This will be the main function to run the service and will be called from te `entrypoint.py`.
