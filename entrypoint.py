import importlib
import logging
import sys
from typing import List, Optional

logging.basicConfig(
    stream=sys.stdout,
    format='[%(levelname)s] %(asctime)s %(name)s {%(module)s} | %(message)s',
    level=logging.DEBUG)

available_service_names = ["bluetooth_server", "light_controller", "wifi_scanner", "analog_sensors_service", "error_reporting_service"]
USAGE = """entrypoint.py defines the entrypoint to execute any of the associated services.

Usage: 
  python3 entrypoint.py <service_name>
  python3 entrypoint.py help
  
service_name    Any of: {}
""".format(', '.join(available_service_names))


def run(service_name: str, args: Optional[List[str]] = None):
    app = importlib.import_module("{}.app".format(service_name))
    if args is not None:
        app.run(args)
    else:
        app.run()


def show_help(exit_code=1):
    print(USAGE)
    sys.exit(exit_code)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Missing 'service' argument!\n")
        show_help()

    if sys.argv[1] == "help":
        show_help(0)

    if sys.argv[1] not in available_service_names:
        print("Invalid service!\n")
        show_help()

    args = sys.argv[2:] if len(sys.argv) > 2 else None

    run(sys.argv[1], args)
