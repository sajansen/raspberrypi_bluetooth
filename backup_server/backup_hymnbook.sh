#!/bin/bash
#Purpose = Backup of sajansen server backup files

SSH_USERNAME="backups"
SSH_DOMAIN="sajansen.nl"
SSH_PORT=4550
BACKUP_OUTPUT_DIR="/home/pi/Backups/${SSH_DOMAIN}"
MAX_AMOUNT_OF_BACKUP_TO_KEEP=7

function removeOldBackups() {
  FILE_SELECTOR_QUERY="$1"
  BACKUP_LIST=($(ls "${BACKUP_OUTPUT_DIR}" | grep -oE "${FILE_SELECTOR_QUERY}[^ ]*?" | sort -r)) || exit 1
  BACKUP_LIST_COUNTER=${MAX_AMOUNT_OF_BACKUP_TO_KEEP}
  while [ ${BACKUP_LIST_COUNTER} -lt ${#BACKUP_LIST[@]} ]; do
    BACKUP_FILE_TO_REMOVE="${BACKUP_OUTPUT_DIR}/${BACKUP_LIST[BACKUP_LIST_COUNTER]}"
    BACKUP_LIST_COUNTER=$((BACKUP_LIST_COUNTER + 1))
    echo "Removing old backup file #${BACKUP_LIST_COUNTER}: ${BACKUP_FILE_TO_REMOVE}"
    rm "${BACKUP_FILE_TO_REMOVE}"
  done
}

function downloadLatestBackup() {
  mkdir -p "${BACKUP_OUTPUT_DIR}"
  echo "Downloading latest backup from ${SSH_DOMAIN} to ${BACKUP_OUTPUT_DIR}..."
  scp -P ${SSH_PORT} ${SSH_USERNAME}@${SSH_DOMAIN}:"$(ssh -p ${SSH_PORT} ${SSH_USERNAME}@${SSH_DOMAIN} 'ls -t /home/backups/hymnbook/db_* | head -1')" ${BACKUP_OUTPUT_DIR} || exit 1
  echo "Done!"
}

downloadLatestBackup
removeOldBackups "db_hymnbook"
