#!/bin/bash
#Purpose = Execute all backup scripts in this directory
# crontab -e
# 0 3 * * * /workingdirectory/main.sh

BACKUP_SCRIPTS=("backup_rehobothkerkwoerden.sh" "backup_jschedule.sh" "backup_hymnbook.sh" "backup_pi.sh")

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"

echo "[main] Starting backup scripts"

for SCRIPT in "${BACKUP_SCRIPTS[@]}"; do
  echo "[main] => Executing script: ${SCRIPT}..."
  . "${SCRIPT_DIR}/${SCRIPT}"
done

echo "[main] Backup of all scripts done!"
