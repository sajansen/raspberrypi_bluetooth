#!/bin/bash
#Purpose = Backup PI files to sajansen server

SSH_USERNAME="backups"
SSH_DOMAIN="sajansen.nl"
SSH_PORT=4550
BACKUP_UPLOAD_DIR="/home/backups/pi"
MAX_AMOUNT_OF_BACKUP_TO_KEEP=7
SCRIPT_PATH=$(dirname $(realpath -s $0))

function removeOldBackups() {
  FILE_SELECTOR_QUERY="$1"
  echo "Check removal of old backup files for '${FILE_SELECTOR_QUERY}'"

  BACKUP_LIST=($(echo $FILES | grep -oE "${FILE_SELECTOR_QUERY}[^ ]*?" | sort -r))
  BACKUP_LIST_COUNTER=${MAX_AMOUNT_OF_BACKUP_TO_KEEP}

  while [ ${BACKUP_LIST_COUNTER} -lt ${#BACKUP_LIST[@]} ]; do
    BACKUP_FILE_TO_REMOVE="${BACKUP_UPLOAD_DIR}/${BACKUP_LIST[BACKUP_LIST_COUNTER]}"
    BACKUP_LIST_COUNTER=$((BACKUP_LIST_COUNTER + 1))
    echo "Removing old backup file #${BACKUP_LIST_COUNTER}: ${BACKUP_FILE_TO_REMOVE}"
    ssh ${SSH_USERNAME}@${SSH_DOMAIN} -p ${SSH_PORT} rm "${BACKUP_FILE_TO_REMOVE}"
  done
}

function uploadLatestBackup() {
  DATE=$(date +"%Y-%m-%d_%H%M%S")
  echo "Uploading latest PI backups to ${SSH_DOMAIN} to ${BACKUP_UPLOAD_DIR}..."
  scp -P ${SSH_PORT} "${SCRIPT_PATH}/../analog_sensors_service/app.db" ${SSH_USERNAME}@${SSH_DOMAIN}:"${BACKUP_UPLOAD_DIR}/analog_sensors_service_${DATE}.db"
  scp -P ${SSH_PORT} "${SCRIPT_PATH}/../wifi_scanner/app.db" ${SSH_USERNAME}@${SSH_DOMAIN}:"${BACKUP_UPLOAD_DIR}/wifi_scanner_${DATE}.db"
  scp -P ${SSH_PORT} "${SCRIPT_PATH}/../../raspberrypi_http/app.db" ${SSH_USERNAME}@${SSH_DOMAIN}:"${BACKUP_UPLOAD_DIR}/raspberrypi_http_${DATE}.db"
  scp -P ${SSH_PORT} "${SCRIPT_PATH}/../../raspberrypi-services/app.db" ${SSH_USERNAME}@${SSH_DOMAIN}:"${BACKUP_UPLOAD_DIR}/raspberrypi-services_${DATE}.db"
  echo "Done!"
}

uploadLatestBackup
FILES=$(ssh ${SSH_USERNAME}@${SSH_DOMAIN} -p ${SSH_PORT} ls ${BACKUP_UPLOAD_DIR}) || exit 1
removeOldBackups "analog_sensors_service_"
removeOldBackups "wifi_scanner_"
removeOldBackups "raspberrypi_http_"
removeOldBackups "raspberrypi-services_"
