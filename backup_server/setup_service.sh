#!/bin/bash

SCRIPTPATH="$(
  cd "$(dirname "$0")"
  pwd -P
)"
CRONJOB="0 4 * * * ${SCRIPTPATH}/main.sh | /usr/bin/logger -t backup_service"

echo "Adding to crontab"
crontab -l |
  grep -v -F "backup_service" |   # Remove any possible existing line for this service and
  {
    cat
    echo "${CRONJOB}"                 # append this service CRONJOb to the end of the crontab
  } |
  crontab -

echo "Done :)"
